package org.processmining.iskra.hammocks;

import org.junit.Test;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by Ivan on 29.07.2016.
 */
public class DominatorsFinderTest_find {
    DominatorsFinder finder = new DominatorsFinder();

    @Test
    public void find() {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);

        Dominators dominators = finder.find(petrinet);
        Map<Transition, Set<Transition>> postDominators = dominators.getPostDominators();
        Map<Transition, Set<Transition>> preDominators = dominators.getPreDominators();

        Map<Transition, Set<Transition>> correctPreDominators = new HashMap<>();

        Set<Transition> preDominatorsA = new HashSet<>();
        preDominatorsA.add(a);
        correctPreDominators.put(a, preDominatorsA);

        Set<Transition> preDominatorsB = new HashSet<>();
        preDominatorsB.add(b);
        preDominatorsB.add(a);
        correctPreDominators.put(b, preDominatorsB);

        Set<Transition> preDominatorsC = new HashSet<>();
        preDominatorsC.add(c);
        preDominatorsC.add(a);
        correctPreDominators.put(c, preDominatorsC);

        Set<Transition> preDominatorsD = new HashSet<>();
        preDominatorsD.add(d);
        preDominatorsD.add(a);
        preDominatorsD.add(b);
        correctPreDominators.put(d, preDominatorsD);

        Set<Transition> preDominatorsE = new HashSet<>();
        preDominatorsE.add(e);
        preDominatorsE.add(a);
        correctPreDominators.put(e, preDominatorsE);

        Map<Transition, Set<Transition>> correctPostDominators = new HashMap<>();

        Set<Transition> postDominatorsA = new HashSet<>();
        postDominatorsA.add(a);
        postDominatorsA.add(e);
        correctPostDominators.put(a, postDominatorsA);

        Set<Transition> postDominatorsB = new HashSet<>();
        postDominatorsB.add(b);
        postDominatorsB.add(d);
        postDominatorsB.add(e);
        correctPostDominators.put(b, postDominatorsB);

        Set<Transition> postDominatorsC = new HashSet<>();
        postDominatorsC.add(c);
        postDominatorsC.add(e);
        correctPostDominators.put(c, postDominatorsC);

        Set<Transition> postDominatorsD = new HashSet<>();
        postDominatorsD.add(d);
        postDominatorsD.add(e);
        correctPostDominators.put(d, postDominatorsD);

        Set<Transition> postDominatorsE = new HashSet<>();
        postDominatorsE.add(e);
        correctPostDominators.put(e, postDominatorsE);

        assertEquals(correctPreDominators, preDominators);
        assertEquals(correctPostDominators, postDominators);
    }

    @Test
    public void find2() {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(initialPlace, b);
        petrinet.addArc(a, p1);
        petrinet.addArc(b, p2);
        petrinet.addArc(p1, c);
        petrinet.addArc(p1, d);
        petrinet.addArc(c, p2);
        petrinet.addArc(d, p2);
        petrinet.addArc(p2, e);
        petrinet.addArc(e, finalPlace);

        Dominators dominators = finder.find(petrinet);
        Map<Transition, Set<Transition>> postDominators = dominators.getPostDominators();
        Map<Transition, Set<Transition>> preDominators = dominators.getPreDominators();

        Map<Transition, Set<Transition>> correctPreDominators = new HashMap<>();

        Set<Transition> preDominatorsA = new HashSet<>();
        preDominatorsA.add(a);
        correctPreDominators.put(a, preDominatorsA);

        Set<Transition> preDominatorsB = new HashSet<>();
        preDominatorsB.add(b);
        correctPreDominators.put(b, preDominatorsB);

        Set<Transition> preDominatorsC = new HashSet<>();
        preDominatorsC.add(c);
        preDominatorsC.add(a);
        correctPreDominators.put(c, preDominatorsC);

        Set<Transition> preDominatorsD = new HashSet<>();
        preDominatorsD.add(d);
        preDominatorsD.add(a);
        correctPreDominators.put(d, preDominatorsD);

        Set<Transition> preDominatorsE = new HashSet<>();
        preDominatorsE.add(e);
        correctPreDominators.put(e, preDominatorsE);

        Map<Transition, Set<Transition>> correctPostDominators = new HashMap<>();

        Set<Transition> postDominatorsA = new HashSet<>();
        postDominatorsA.add(a);
        postDominatorsA.add(e);
        correctPostDominators.put(a, postDominatorsA);

        Set<Transition> postDominatorsB = new HashSet<>();
        postDominatorsB.add(b);
        postDominatorsB.add(e);
        correctPostDominators.put(b, postDominatorsB);

        Set<Transition> postDominatorsC = new HashSet<>();
        postDominatorsC.add(c);
        postDominatorsC.add(e);
        correctPostDominators.put(c, postDominatorsC);

        Set<Transition> postDominatorsD = new HashSet<>();
        postDominatorsD.add(d);
        postDominatorsD.add(e);
        correctPostDominators.put(d, postDominatorsD);

        Set<Transition> postDominatorsE = new HashSet<>();
        postDominatorsE.add(e);
        correctPostDominators.put(e, postDominatorsE);

        assertEquals(correctPreDominators, preDominators);
        assertEquals(correctPostDominators, postDominators);
    }
}