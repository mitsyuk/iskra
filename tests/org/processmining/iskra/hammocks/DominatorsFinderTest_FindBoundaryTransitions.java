package org.processmining.iskra.hammocks;

import org.junit.Test;
import org.processmining.framework.util.Pair;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by Ivan on 29.07.2016.
 */
public class DominatorsFinderTest_FindBoundaryTransitions {
    private DominatorsFinder finder = new DominatorsFinder();

    @Test
    public void findBoundaryTransitions_Correct() throws Exception {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);

        Pair<Set<Transition>, Set<Transition>> boundaries = finder.findBoundaryTransitions(petrinet);
        Set<Transition> initialTransitions = boundaries.getFirst();
        Set<Transition> finalTransitions = boundaries.getSecond();

        Set<Transition> correctInitialTransitions = new HashSet<>();
        correctInitialTransitions.add(a);

        Set<Transition> correctFinalTransitions = new HashSet<>();
        correctFinalTransitions.add(e);

        assertEquals(correctInitialTransitions, initialTransitions);
        assertEquals(correctFinalTransitions, finalTransitions);
    }

    @Test
    public void findBoundaryTransitions_Correct_MultipleInitialTransitions() throws Exception {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition a1 = petrinet.addTransition("A1");
        Transition a2 = petrinet.addTransition("A2");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(initialPlace, a1);
        petrinet.addArc(initialPlace, a2);

        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);
        petrinet.addArc(a1, p1);
        petrinet.addArc(a2, p1);

        Pair<Set<Transition>, Set<Transition>> boundaries = finder.findBoundaryTransitions(petrinet);
        Set<Transition> initialTransitions = boundaries.getFirst();
        Set<Transition> finalTransitions = boundaries.getSecond();

        Set<Transition> correctInitialTransitions = new HashSet<>();
        correctInitialTransitions.add(a);
        correctInitialTransitions.add(a1);
        correctInitialTransitions.add(a2);

        Set<Transition> correctFinalTransitions = new HashSet<>();
        correctFinalTransitions.add(e);

        assertEquals(correctInitialTransitions, initialTransitions);
        assertEquals(correctFinalTransitions, finalTransitions);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findBoundaryTransitions_Incorrect_2InitialPlaces() throws Exception {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place initialPlace2 = petrinet.addPlace("initial2");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(initialPlace2, c);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);

        finder.findBoundaryTransitions(petrinet);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findBoundaryTransitions_Incorrect_2FinalPlaces() throws Exception {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace2 = petrinet.addPlace("final2");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);
        petrinet.addArc(e, finalPlace2);

        finder.findBoundaryTransitions(petrinet);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findBoundaryTransitions_Incorrect_DanglingPlace() throws Exception {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");
        Place danglingPlace = petrinet.addPlace("dangling");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);

        finder.findBoundaryTransitions(petrinet);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findBoundaryTransitions_Incorrect_WithoutInitialPlace() throws Exception {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);

        finder.findBoundaryTransitions(petrinet);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findBoundaryTransitions_Incorrect_WithoutFinalPlaces() throws Exception {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);

        finder.findBoundaryTransitions(petrinet);
    }


    @Test(expected = IllegalArgumentException.class)
    public void findBoundaryTransitions_Incorrect_DanglingTransition() throws Exception {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, d);
        petrinet.addArc(e, finalPlace);

        Pair<Set<Transition>, Set<Transition>> boundaries = finder.findBoundaryTransitions(petrinet);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findBoundaryTransitions_Incorrect_DanglingTransition2() throws Exception {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(a, finalPlace);

        Pair<Set<Transition>, Set<Transition>> boundaries = finder.findBoundaryTransitions(petrinet);
    }
}