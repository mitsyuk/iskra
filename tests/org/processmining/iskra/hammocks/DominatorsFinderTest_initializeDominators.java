package org.processmining.iskra.hammocks;

import org.junit.Test;
import org.processmining.framework.util.Pair;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by Ivan on 29.07.2016.
 */
public class DominatorsFinderTest_initializeDominators {
    DominatorsFinder finder = new DominatorsFinder();

    @Test
    public void initializeDominators() throws Exception {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);

        Set<Transition> initialTransitions = new HashSet<>();
        initialTransitions.add(a);

        Set<Transition> finalTransitions = new HashSet<>();
        finalTransitions.add(e);

        Pair<Map<Transition, Set<Transition>>, Map<Transition, Set<Transition>>> initialSearchState =
                finder.initializeDominators(petrinet, initialTransitions, finalTransitions);

        Map<Transition, Set<Transition>> preDominators = initialSearchState.getFirst();
        Map<Transition, Set<Transition>> postDominators = initialSearchState.getSecond();

        Map<Transition, Set<Transition>> correctPreDominators = new HashMap<>();
        Map<Transition, Set<Transition>> correctPostDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            correctPreDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            correctPostDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> preDominatorsForA = new HashSet<>();
        preDominatorsForA.add(a);
        correctPreDominators.put(a, preDominatorsForA);

        Set<Transition> postDominatorsForE = new HashSet<>();
        postDominatorsForE.add(e);
        correctPostDominators.put(e, postDominatorsForE);

        assertEquals(correctPreDominators, preDominators);
        assertEquals(correctPostDominators, postDominators);
    }

    @Test
    public void initializeDominators_MultipleInputTransitions() throws Exception {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition a1 = petrinet.addTransition("A1");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(initialPlace, a1);
        petrinet.addArc(a, p1);
        petrinet.addArc(a1, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);

        Set<Transition> initialTransitions = new HashSet<>();
        initialTransitions.add(a);
        initialTransitions.add(a1);

        Set<Transition> finalTransitions = new HashSet<>();
        finalTransitions.add(e);

        Pair<Map<Transition, Set<Transition>>, Map<Transition, Set<Transition>>> initialSearchState =
                finder.initializeDominators(petrinet, initialTransitions, finalTransitions);

        Map<Transition, Set<Transition>> preDominators = initialSearchState.getFirst();
        Map<Transition, Set<Transition>> postDominators = initialSearchState.getSecond();

        Map<Transition, Set<Transition>> correctPreDominators = new HashMap<>();
        Map<Transition, Set<Transition>> correctPostDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            correctPreDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            correctPostDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> preDominatorsForA = new HashSet<>();
        preDominatorsForA.add(a);
        correctPreDominators.put(a, preDominatorsForA);

        Set<Transition> preDominatorsForA1 = new HashSet<>();
        preDominatorsForA1.add(a1);
        correctPreDominators.put(a1, preDominatorsForA1);

        Set<Transition> postDominatorsForE = new HashSet<>();
        postDominatorsForE.add(e);
        correctPostDominators.put(e, postDominatorsForE);

        assertEquals(correctPreDominators, preDominators);
        assertEquals(correctPostDominators, postDominators);
    }

    @Test
    public void initializeDominators_MultipleFinalTransitions() throws Exception {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");
        Transition e1 = petrinet.addTransition("E1");
        Transition e2 = petrinet.addTransition("E2");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);
        petrinet.addArc(p3, e1);
        petrinet.addArc(p3, e2);
        petrinet.addArc(e1, finalPlace);
        petrinet.addArc(e2, finalPlace);

        Set<Transition> initialTransitions = new HashSet<>();
        initialTransitions.add(a);

        Set<Transition> finalTransitions = new HashSet<>();
        finalTransitions.add(e);
        finalTransitions.add(e1);
        finalTransitions.add(e2);

        Pair<Map<Transition, Set<Transition>>, Map<Transition, Set<Transition>>> initialSearchState =
                finder.initializeDominators(petrinet, initialTransitions, finalTransitions);

        Map<Transition, Set<Transition>> preDominators = initialSearchState.getFirst();
        Map<Transition, Set<Transition>> postDominators = initialSearchState.getSecond();

        Map<Transition, Set<Transition>> correctPreDominators = new HashMap<>();
        Map<Transition, Set<Transition>> correctPostDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            correctPreDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            correctPostDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> preDominatorsForA = new HashSet<>();
        preDominatorsForA.add(a);
        correctPreDominators.put(a, preDominatorsForA);

        Set<Transition> postDominatorsForE = new HashSet<>();
        postDominatorsForE.add(e);
        correctPostDominators.put(e, postDominatorsForE);

        Set<Transition> postDominatorsForE1 = new HashSet<>();
        postDominatorsForE1.add(e1);
        correctPostDominators.put(e1, postDominatorsForE1);

        Set<Transition> postDominatorsForE2 = new HashSet<>();
        postDominatorsForE2.add(e2);
        correctPostDominators.put(e2, postDominatorsForE2);

        assertEquals(correctPreDominators, preDominators);
        assertEquals(correctPostDominators, postDominators);
    }

    @Test
    public void initializeDominators_MultipleInitialAndFinalTransitions() throws Exception {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition a1 = petrinet.addTransition("A1");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");
        Transition e1 = petrinet.addTransition("E1");
        Transition e2 = petrinet.addTransition("E2");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(initialPlace, a1);
        petrinet.addArc(a, p1);
        petrinet.addArc(a1, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);
        petrinet.addArc(p3, e1);
        petrinet.addArc(p3, e2);
        petrinet.addArc(e1, finalPlace);
        petrinet.addArc(e2, finalPlace);

        Set<Transition> initialTransitions = new HashSet<>();
        initialTransitions.add(a);
        initialTransitions.add(a1);

        Set<Transition> finalTransitions = new HashSet<>();
        finalTransitions.add(e);
        finalTransitions.add(e1);
        finalTransitions.add(e2);

        Pair<Map<Transition, Set<Transition>>, Map<Transition, Set<Transition>>> initialSearchState =
                finder.initializeDominators(petrinet, initialTransitions, finalTransitions);

        Map<Transition, Set<Transition>> preDominators = initialSearchState.getFirst();
        Map<Transition, Set<Transition>> postDominators = initialSearchState.getSecond();

        Map<Transition, Set<Transition>> correctPreDominators = new HashMap<>();
        Map<Transition, Set<Transition>> correctPostDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            correctPreDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            correctPostDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> preDominatorsForA = new HashSet<>();
        preDominatorsForA.add(a);
        correctPreDominators.put(a, preDominatorsForA);

        Set<Transition> preDominatorsForA1 = new HashSet<>();
        preDominatorsForA1.add(a1);
        correctPreDominators.put(a1, preDominatorsForA1);

        Set<Transition> postDominatorsForE = new HashSet<>();
        postDominatorsForE.add(e);
        correctPostDominators.put(e, postDominatorsForE);

        Set<Transition> postDominatorsForE1 = new HashSet<>();
        postDominatorsForE1.add(e1);
        correctPostDominators.put(e1, postDominatorsForE1);

        Set<Transition> postDominatorsForE2 = new HashSet<>();
        postDominatorsForE2.add(e2);
        correctPostDominators.put(e2, postDominatorsForE2);

        assertEquals(correctPreDominators, preDominators);
        assertEquals(correctPostDominators, postDominators);
    }


}