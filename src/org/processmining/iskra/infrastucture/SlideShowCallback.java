package org.processmining.iskra.infrastucture;

/**
 * Callback interface for SlideShower
 *
 * @author Dmitriy Yakovlev
 */
public interface SlideShowCallback {
    /**
     * Something happened in SlideShow
     *
     * @param o object on which callback happened
     */
    void itHappens(Object o);
}
