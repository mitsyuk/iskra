package org.processmining.iskra.infrastucture;

import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.IskraResult;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;

/**
 * Created by Ivan Shugurov on 08.01.2015.
 */
public interface RepairChain {

    boolean USE_PROVIDED_OBJECTS = false;

    IskraResult execute(PluginContext context,
                        ComposedModel initialModel, TransEvClassMapping mapping) throws Exception;
}
