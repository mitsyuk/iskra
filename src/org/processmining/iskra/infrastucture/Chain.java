package org.processmining.iskra.infrastucture;

import java.lang.annotation.*;

/**
 * Created by Ivan on 26.03.2015.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Chain {
    String value();
}
