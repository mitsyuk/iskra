package org.processmining.iskra.infrastucture.impl;

import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.infrastucture.AbstractIskraRepairChainArray;
import org.processmining.iskra.types.AbstractIskraCombinedResult;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.impl.IskraCombinedResult;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * The only implementation of AbstractIskraRepairChainArray
 *
 * @author Ivan Shugurov
 */
public class IndependentIskraRepairChain implements AbstractIskraRepairChainArray {

    private final List<IskraRepairChainImpl> pack = new CopyOnWriteArrayList<IskraRepairChainImpl>();

    @Override
    public List<IskraRepairChainImpl> getPack() {
        return pack;
    }

    @Override
    public AbstractIskraCombinedResult execute(PluginContext context,
                                               ComposedModel initialModel,
                                               double fitnessThreshold, TransEvClassMapping mapping) throws Exception {
        AbstractIskraCombinedResult result = new IskraCombinedResult(pack);
        for (IskraRepairChainImpl chain : pack) {
            result.getResults().put(chain, chain.execute(context,
                    initialModel, mapping));
        }
        return result;
    }
}
