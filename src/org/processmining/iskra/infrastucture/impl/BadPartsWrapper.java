package org.processmining.iskra.infrastucture.impl;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;

import java.awt.*;
import java.util.*;
import java.util.List;

import static org.processmining.iskra.infrastucture.RepairChain.USE_PROVIDED_OBJECTS;
import static org.processmining.iskra.utils.AcceptingPetriNetMerger.mergePetriNetArray;

/**
 * Created by Ivan on 18.04.2016.
 */
public class BadPartsWrapper {
    public WrappingResult wrapBadParts(
            PluginContext context,
            AcceptingPetriNetArray decomposedNets,
            Collection<AcceptingPetriNet> badParts) {

        List<AcceptingPetriNetArray> badClusters = new ArrayList<>();

        Map<AcceptingPetriNet, Petrinet> initialBadPartToCopiedPartMap = new HashMap<>();

        for (AcceptingPetriNet acceptingBadPart : badParts) {
            Petrinet badPart = acceptingBadPart.getNet();

            Petrinet copiedBadPart = PetrinetFactory.clonePetrinet(badPart);
            initialBadPartToCopiedPartMap.put(acceptingBadPart, copiedBadPart);

            if (USE_PROVIDED_OBJECTS) {
                for (Transition transition : copiedBadPart.getTransitions()) {
                    AttributeMap attributeMap = transition.getAttributeMap();
                    attributeMap.put(AttributeMap.FILLCOLOR, Color.RED);
                }
            }
        }

        for (AcceptingPetriNet badPart : badParts) {
            AcceptingPetriNetArray partWrapper = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();
            AcceptingPetriNetArray copiedWrapper = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

            for (int i = 0; i < decomposedNets.getSize(); i++) {
                AcceptingPetriNet net = decomposedNets.getNet(i);

                if (net == badPart || checkIntersection(net.getNet().getTransitions(), badPart.getNet().getTransitions())) {
                    partWrapper.addNet(net);
                    decomposedNets.removeNet(i);
                    i--;

                    AcceptingPetriNet copy = AcceptingPetriNetFactory.createAcceptingPetriNet(initialBadPartToCopiedPartMap.get(badPart));
                    copiedWrapper.addNet(copy);
                }
            }

            if (partWrapper.getSize() != 0) {
                badClusters.add(partWrapper);
            }
        }

        AcceptingPetriNetArray mergedClusters = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();
        for (AcceptingPetriNetArray netArray : badClusters) {
            AcceptingPetriNet mergedCluster = mergePetriNetArray(context, netArray);

            //mergedCluster = RemoveStructuralRedundantPlaces.reduceDefault(mergedCluster);         TODO удаление повторяющихся состояний
            mergedClusters.addNet(mergedCluster);
        }


        mergedClusters = eliminateIntersections(context, mergedClusters);

        return new WrappingResult(mergedClusters, decomposedNets);
    }

    protected AcceptingPetriNetArray eliminateIntersections(PluginContext context, AcceptingPetriNetArray mergedClusters) {
        boolean withoutIntersections;

        do {
            boolean[] partsMergedOnThisStep = new boolean[mergedClusters.getSize()];
            Arrays.fill(partsMergedOnThisStep, false);
            withoutIntersections = true;

            AcceptingPetriNetArray biggerWrappers = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

            for (int i = 0; i < mergedClusters.getSize(); i++) {
                if (!partsMergedOnThisStep[i]) {
                    AcceptingPetriNetArray biggerWrapper = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

                    AcceptingPetriNet acceptingPetriNet1 = mergedClusters.getNet(i);
                    Petrinet petrinet1 = acceptingPetriNet1.getNet();
                    biggerWrapper.addNet(acceptingPetriNet1);

                    for (int j = i + 1; j < mergedClusters.getSize(); j++) {
                        if (!partsMergedOnThisStep[j]) {
                            AcceptingPetriNet acceptingPetriNet2 = mergedClusters.getNet(j);
                            Petrinet petrinet2 = acceptingPetriNet2.getNet();

                            if (checkIntersection(petrinet1.getTransitions(), petrinet2.getTransitions())) {
                                biggerWrapper.addNet(acceptingPetriNet2);
                                partsMergedOnThisStep[j] = true;
                                withoutIntersections = false;
                            }
                        }
                    }

                    AcceptingPetriNet mergedBiggerWrapper = mergePetriNetArray(context, biggerWrapper);
                    //mergedBiggerWrapper = RemoveStructuralRedundantPlaces.reduceDefault(mergedBiggerWrapper);  //  TODO удаляю повторяющиеся состояния
                    biggerWrappers.addNet(mergedBiggerWrapper);

                }
            }

            mergedClusters = biggerWrappers;

        } while (!withoutIntersections);
        return mergedClusters;
    }

    protected boolean checkIntersection(Collection<Transition> transitions1, Collection<Transition> transitions2) {
        for (Transition transition : transitions1) {
            if (!transition.isInvisible()) {
                for (Transition transition2 : transitions2) {
                    if (transition.getLabel().equals(transition2.getLabel())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
