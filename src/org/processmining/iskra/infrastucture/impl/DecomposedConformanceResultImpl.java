package org.processmining.iskra.infrastucture.impl;

import org.processmining.iskra.types.DecomposedConformanceResult;

import java.util.ArrayList;
import java.util.List;

/**
 * The only implementation of DecomposedConformanceResult
 *
 * @author Ivan Shugurov
 */
public class DecomposedConformanceResultImpl implements DecomposedConformanceResult {
    private List<Double> conformanceList;  //TODO don't like this list. is it better to have pairs (net part, conformance)?
    private double fitnessThreshold;

    public DecomposedConformanceResultImpl(List<Double> conformanceList, double conformanceThreshold) {
        if (conformanceThreshold < 0 || conformanceThreshold > 1) {
            throw new IllegalArgumentException("Threshold must be bigger or equal to 0 and less or equal to 1");
        }

        this.fitnessThreshold = conformanceThreshold;
        this.conformanceList = new ArrayList<>(conformanceList);
    }

    @Override
    public boolean isBadPart(int partIndex) {
        return getConformance(partIndex) < fitnessThreshold;
    }

    @Override
    public double getConformance(int partIndex) {
        return conformanceList.get(partIndex);
    }

    @Override
    public int size() {
        return conformanceList.size();
    }

    @Override
    public double getFitnessThreshold() {
        return fitnessThreshold;
    }
}
