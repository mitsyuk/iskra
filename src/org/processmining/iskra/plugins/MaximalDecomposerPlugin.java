package org.processmining.iskra.plugins;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.iskra.utils.MaximalDecomposer;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

/**
 * Created by Ivan on 25.04.2015.
 */

@Plugin(
        name = "Maximal decomposition",
        returnLabels = {"Decomposition"},
        returnTypes = {AcceptingPetriNetArray.class},
        parameterLabels = {"Petri net", "Accepting Petri net"}
)
public class MaximalDecomposerPlugin {

    @PluginVariant(requiredParameterLabels = {0})
    @UITopiaVariant(
            affiliation = "HSE PAIS Lab",
            author = "Alex Mitsyuk, Ivan Shugurov",
            email = "amitsyuk@hse.ruamitsyuk@hse.ru")
    public AcceptingPetriNetArray decompose(PluginContext context, Petrinet petrinet) {
        MaximalDecomposer maximalDecomposer = new MaximalDecomposer();
        return maximalDecomposer.decompose(petrinet);
    }

    @PluginVariant(requiredParameterLabels = {1})
    @UITopiaVariant(
            affiliation = "HSE PAIS Lab",
            author = "Alex Mitsyuk, Ivan Shugurov",
            email = "amitsyuk@hse.ru")
    public AcceptingPetriNetArray decompose(PluginContext context, AcceptingPetriNet petrinet) {
        MaximalDecomposer maximalDecomposer = new MaximalDecomposer();
        return maximalDecomposer.decompose(petrinet);
    }
}
