package org.processmining.iskra.plugins;

import org.processmining.contexts.uitopia.annotations.UIExportPlugin;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.iskra.infrastucture.IskraTimingResult;
import org.processmining.iskra.types.FinalEvaluationResult;
import org.processmining.iskra.types.IskraResult;

import java.io.*;

/**
 * Created by Ivan Shugurov on 12.11.2014.
 */
@Plugin(name = "Iskra export", returnLabels = {}, returnTypes = {}, parameterLabels = {
        "Iskra result", "File"}, userAccessible = true)
@UIExportPlugin(description = "CSV File", extension = "csv")
public class ExportIskraResultAsCSVPlugin {
    private static final String DECOMPOSER_NAME_TAG = "Decomposer name";
    private static final String REPAIRER_NAME_TAG = "Repairer name";
    private static final String CHAIN_NAME_TAG = "Chain name";
    private static final String INITIAL_FITNESS_TAG = "Initial fitness";
    private static final String FINAL_FITNESS_TAG = "Final fitness";
    private static final String INITIAL_CONFORMANCE_TAG = "Initial conformance";
    private static final String FINAL_CONFORMANCE_TAG = "Final conformance";
    private static final String INITIAL_PRECISION_TAG = "Initial precision";
    private static final String FINAL_PRECISION_TAG = "Final precision";
    private static final String INITIAL_GENERALIZATION_TAG = "Initial generalization";
    private static final String FINAL_GENERALIZATION_TAG = "Final generalization";
    private static final String INITIAL_SIMPLICITY_TAG = "Initial simplicity";
    private static final String FINAL_SIMPLICITY_TAG = "Final simplicity";
    private static final String SIMILARITY_TAG = "Similarity";
    private static final String TOTAL_EVALUATION_TAG = "Total evaluation";
    private static final String DECOMPOSITION_TIME_TAG = "Decomposition time";
    private static final String PARTIAL_CONFORMANCE_CHECKING_TIME_TAG = "Conformance checking time for a decomposed model";
    private static final String REPAIR_TIME_TAG = "Repair time";
    private static final String COMPOSITION_TIME_TAG = "Composition time";
    private static final String FINAL_EVALUATION_TIME_TAG = "Final evaluation time";
    private static final String TOTAL_TIME_TAG = "Total time";
    private static final String SEPARATOR = ",";
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    @PluginVariant(variantLabel = "", requiredParameterLabels = {0, 1})
    public void export(PluginContext context, IskraResult result, File file) throws IOException {
        Writer writer = new PrintWriter(file);

        try {
            writeHeader(writer);
            writerRow(writer, result);
            writer.flush();
        } finally {
            tryToCloseStream(writer);
        }
    }

    private void writerRow(Writer writer, IskraResult result) throws IOException {
        FinalEvaluationResult evaluationResult = result.getEvaluationResult();
        IskraTimingResult timingResult = result.getTimingResult();
        String decomposerName = result.getDecomposerName();
        String repairerName = result.getRepairerName();
        writeRow(writer, evaluationResult, timingResult, decomposerName, repairerName);
    }

    private void writeRow(Writer writer, FinalEvaluationResult result, IskraTimingResult timingResult, String decomposerName, String repairerName) throws IOException {

        writeWithDelimiter(writer, decomposerName);
        writeWithDelimiter(writer, repairerName);
        writeWithDelimiter(writer, result.getChainName());

        double initialFitness = result.getInitialModelFitness();
        writeWithDelimiter(writer, initialFitness);
        double finalFitness = result.getRepairedModelFitness();
        writeWithDelimiter(writer, finalFitness);

        double initialConformance = result.getInitialModelConformance();
        writeWithDelimiter(writer, initialConformance);
        double finalConformance = result.getRepairedModelConformance();
        writeWithDelimiter(writer, finalConformance);

        double initialPrecision = result.getInitialModelPrecision();
        writeWithDelimiter(writer, initialPrecision);
        double finalPrecision = result.getRepairedModelPrecision();
        writeWithDelimiter(writer, finalPrecision);

        double initialGeneralization = result.getInitialModelGeneralization();
        writeWithDelimiter(writer, initialGeneralization);
        double finalGeneralization = result.getRepairedModelGeneralization();
        writeWithDelimiter(writer, finalGeneralization);

        double initialSimplicity = result.getInitialModelComplexity();
        writeWithDelimiter(writer, initialSimplicity);
        double finalSimplicity = result.getRepairedModelComplexity();
        writeWithDelimiter(writer, finalSimplicity);

        double similarity = result.getSimilarity();
        writeWithDelimiter(writer, similarity);

        double totalEvaluation = result.getTotalEvaluation();
        writeWithDelimiter(writer, totalEvaluation);

        double decompositionTime = timingResult.getDecompositionTime();
        writeWithDelimiter(writer, decompositionTime);

        long partialConformanceChecking = timingResult.getPartialConformanceCheckingTime();
        writeWithDelimiter(writer, partialConformanceChecking);

        long repairTime = timingResult.getRepairTime();
        writeWithDelimiter(writer, repairTime);

        long compositionTime = timingResult.getCompositionTime();
        writeWithDelimiter(writer, compositionTime);

        long finalEvaluationTime = timingResult.getFinalEvaluationTime();
        writeWithDelimiter(writer, finalEvaluationTime);

        long totalTime = timingResult.getTotalTime();
        writeWithoutDelimiter(writer, totalTime);

        writer.write(LINE_SEPARATOR);
    }

    private void writeWithoutDelimiter(Writer writer, double value) throws IOException {
        String valueAsString = Double.toString(value);
        writer.write(valueAsString);
    }

    private void writeHeader(Writer writer) throws IOException {
        writeWithDelimiter(writer, DECOMPOSER_NAME_TAG);
        writeWithDelimiter(writer, REPAIRER_NAME_TAG);
        writeWithDelimiter(writer, CHAIN_NAME_TAG);
        writeWithDelimiter(writer, INITIAL_FITNESS_TAG);
        writeWithDelimiter(writer, FINAL_FITNESS_TAG);
        writeWithDelimiter(writer, INITIAL_CONFORMANCE_TAG);
        writeWithDelimiter(writer, FINAL_CONFORMANCE_TAG);
        writeWithDelimiter(writer, INITIAL_PRECISION_TAG);
        writeWithDelimiter(writer, FINAL_PRECISION_TAG);
        writeWithDelimiter(writer, INITIAL_GENERALIZATION_TAG);
        writeWithDelimiter(writer, FINAL_GENERALIZATION_TAG);
        writeWithDelimiter(writer, INITIAL_SIMPLICITY_TAG);
        writeWithDelimiter(writer, FINAL_SIMPLICITY_TAG);
        writeWithDelimiter(writer, SIMILARITY_TAG);
        writeWithDelimiter(writer, TOTAL_EVALUATION_TAG);
        writeWithDelimiter(writer, DECOMPOSITION_TIME_TAG);
        writeWithDelimiter(writer, PARTIAL_CONFORMANCE_CHECKING_TIME_TAG);
        writeWithDelimiter(writer, REPAIR_TIME_TAG);
        writeWithDelimiter(writer, COMPOSITION_TIME_TAG);
        writeWithDelimiter(writer, FINAL_EVALUATION_TIME_TAG);
        writeWithoutDelimiter(writer, TOTAL_TIME_TAG);
        writer.write(LINE_SEPARATOR);
    }

    private void tryToCloseStream(Writer writer) {
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeWithoutDelimiter(Writer writer, String text) throws IOException {
        writer.write(text);
    }

    private void writeWithDelimiter(Writer writer, String text) throws IOException {
        writer.write(text);
        writer.write(SEPARATOR);
    }

    private void writeWithDelimiter(Writer writer, double value) throws IOException {
        String valuesAsString = Double.toString(value);
        writer.write(valuesAsString);
        writer.write(SEPARATOR);
    }

    public void exportToConstantFile(IskraResult result) throws IOException {
        File file = new File("results.csv");
        boolean fileExisted = file.exists();

        FileWriter writer = new FileWriter(file, true);

        try {
            if (!fileExisted) {
                writeHeader(writer);
            }
            writerRow(writer, result);
            writer.flush();
        } finally {
            writer.close();
        }
    }
}
