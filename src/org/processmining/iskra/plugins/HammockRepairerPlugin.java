package org.processmining.iskra.plugins;

import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.iskra.hammocks.HammockRepairer;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.plugins.petrinet.replayresult.PNRepResult;

/**
 * Created by Ivan on 15.08.2016.
 */
public class HammockRepairerPlugin {
    @Plugin(name = "Repair using hammocks", returnLabels = "Repaired model", returnTypes = Petrinet.class,
            parameterLabels = {"Petri net", "Conformance result"})
    @UITopiaVariant(affiliation = "HSE PAIS Lab",
            author = "Alex Mitsyuk, Ivan Shugurov",
            email = "amitsyuk@hse.ru",
            pack = "DecomposedReplay")
    public Petrinet findHammocks(UIPluginContext context, Petrinet petrinet, PNRepResult replayResult, XLog log) {
        return new HammockRepairer().repair(context, petrinet, replayResult, log);
    }
}
