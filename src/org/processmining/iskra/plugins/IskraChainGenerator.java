package org.processmining.iskra.plugins;

import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.iskra.infrastucture.impl.IskraRepairChainImpl;

/**
 * Created by user on 07.07.14.
 */
@Plugin(name = "Iskra chain generator", parameterLabels = {},
        returnLabels = {"Iskra repair chain"}, returnTypes = {IskraRepairChainImpl.class})
public class IskraChainGenerator {
    private static final double DEFAULT_FITNESS_THRESHOLD = 0.75;
    private UIPluginContext context;


    @UITopiaVariant(affiliation = "HSE PAIS Lab",
            author = "Alexey Mitsyuk, Ivan Shugurov, Dmitry Yakovlev",
            email = "amitsyuk" + (char) 0x40 + "hse.ru",
            pack = "DecomposedReplay")
    @PluginVariant(variantLabel = "Iskra chain generator, UI", requiredParameterLabels = {})
    public IskraRepairChainImpl generateUIPlugin(UIPluginContext context) {
        return generateUIwithProm(context);
    }

    public IskraRepairChainImpl generateUIWithDialog() {
//        final List<JComponent> wizardConfig = new ArrayList<JComponent>();
//        final IskraSettingsPanel IskraConfig = new IskraSettingsPanel(null, null, DEFAULT_FITNESS_THRESHOLD);
//        wizardConfig.add(IskraConfig);
//        wizardConfig.add(null);
//        wizardConfig.add(null);
//        PonySpeak dialog = new PonySpeak(wizardConfig, new SlideShowCallback() {
//            @Override
//            public void itHappens(Object o) {
//                if (IskraConfig.equals(o)) {
//                    IskraConfig.verify();
//                    wizardConfig.set(1, IskraConfig.getChosenDecompositionWrap().getSettingsComponent());
//                    wizardConfig.set(2, IskraConfig.getChosenMinerPlugin().getSettingsComponent());
//                }
//            }
//        });
//        dialog.setVisible(true);
//        return dialog.getChain();
        return null;
    }

    public IskraRepairChainImpl generateUIwithProm(UIPluginContext context) {
//        final List<JComponent> wizardConfig = new ArrayList<JComponent>();
//        final IskraSettingsPanel IskraConfig = new IskraSettingsPanel(null, null, DEFAULT_FITNESS_THRESHOLD);
//        SlideShowCallback changer = new SlideShowCallback() {
//            @Override
//            public void itHappens(Object o) {
//                if (IskraConfig.equals(o)) {
//                    wizardConfig.set(1, IskraConfig.getChosenDecompositionWrap().getSettingsComponent());
//                    wizardConfig.set(2, IskraConfig.getChosenMinerPlugin().getSettingsComponent());
//                }
//            }
//        };
//        wizardConfig.add(IskraConfig);
//        wizardConfig.add(null);
//        wizardConfig.add(null);
//        IskraRepairChainImpl result = null;
//        if (SlideShower.show(context, wizardConfig, changer)) {
//            IskraDecomposer decomposer = IskraConfig.getChosenDecompositionWrap();
//            IskraRepairer repairer = IskraConfig.getChosenMinerPlugin();
//            double fitnessThreshold = IskraConfig.getFitnessThreshold();
//            IskraDecomposedConformance selector = new IskraDecomposedConformanceImpl();
//            IskraFinalEvaluation evaluator = new IskraFinalEvaluationImpl();
//            result = new IskraRepairChainImpl(decomposer, repairer, selector, evaluator, fitnessThreshold);
//        }
//        return result;
        return null;
    }
}
