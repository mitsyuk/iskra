package org.processmining.iskra.plugins;

import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.Visualizer;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.iskra.dialogs.UIChainPanel;
import org.processmining.iskra.infrastucture.impl.IskraRepairChainImpl;

import javax.swing.*;

/**
 * Created by user on 07.07.14.
 */
public class VisualizeIskraChain {
    @Plugin(name = "Visualize Iskra Chain",
            returnLabels = {"Visualized Iskra Chain"},
            returnTypes = {JComponent.class},
            parameterLabels = {"Iskra Chain"},
            userAccessible = false)
    @Visualizer
    @PluginVariant(requiredParameterLabels = {0})
    public JComponent visualize(UIPluginContext context, IskraRepairChainImpl chain) {
        return new UIChainPanel(chain);
    }
}
