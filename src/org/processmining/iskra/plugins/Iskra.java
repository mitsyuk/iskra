package org.processmining.iskra.plugins;

import org.deckfour.uitopia.api.event.TaskListener;
import org.deckfour.xes.model.*;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginManager;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.framework.util.Pair;
import org.processmining.iskra.dialogs.IskraSettingsPanel;
import org.processmining.iskra.infrastucture.Chain;
import org.processmining.iskra.infrastucture.RepairChain;
import org.processmining.iskra.pluginwraps.IskraFinalEvaluation;
import org.processmining.iskra.pluginwraps.IskraFinalEvaluationImpl;
import org.processmining.iskra.pluginwraps.conformance.ConformanceCheckingUsingAlignments;
import org.processmining.iskra.pluginwraps.decomposition.Decomposer;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.iskra.pluginwraps.repair.Repairer;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.IskraResult;
import org.processmining.iskra.types.Metrics;
import org.processmining.iskra.types.impl.ComposedModelImpl;
import org.processmining.iskra.utils.BoundaryPlacesFinder;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.semantics.petrinet.Marking;
import org.processmining.processtree.Task;
import ru.hse.pais.shugurov.widgets.panels.SingleChoicePanel;

import javax.swing.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.*;


//// Decomposed Repair Framework

/*
 * @author Alex Mitsyuk (amitsyuk@hse.ru)
 */

@Plugin(name = "Iskra",
        parameterLabels = {"Initial Model (Petri Net)", "Event log"},
        returnLabels = {"Decomposed Replay Results", "Final Model"},
        returnTypes = {IskraResult.class, Petrinet.class},
        userAccessible = true,
        help = "Iskra: Decomposed Repair Framework"
)
public class Iskra extends IskraBase {


    private Collection<IskraDecomposer> allDecomposers;
    private Collection<IskraRepairer> allRepiarers;
    private Map<String, Class<RepairChain>> namesToChainClasses;
    private String selectedChainName;



    // UI - OneChain + One Experiment

    @UITopiaVariant(affiliation = "HSE PAIS Lab",
            author = "Alex Mitsyuk, Ivan Shugurov, Dmitry Yakovlev",
            email = "amitsyuk@hse.ru",
            pack = "DecomposedReplay")
    @PluginVariant(variantLabel = "Iskra main",
    requiredParameterLabels = {0,1})
    public Object[] iskraPlugin(UIPluginContext context,
                                Petrinet petrinet,
                                XLog log) throws Exception {

        this.context = context;
        this.petrinet = petrinet;
        this.log = log;

        Pair<Set<Place>, Set<Place>> boundaryPlaces = new BoundaryPlacesFinder().find(petrinet);
        initialMarking.addAll(boundaryPlaces.getFirst());
        finalMarking.addAll(boundaryPlaces.getSecond());

        int screenNumber = -1;
        TaskListener.InteractionResult interactionResult = TaskListener.InteractionResult.CANCEL;

        ComposedModel composedModel = new ComposedModelImpl(petrinet, log);

        // Plugin setup interface
        //
        while (true) {
            switch (screenNumber) {
                case -1:
                    if(!precheck()) {
                        interactionResult = TaskListener.InteractionResult.CANCEL;
//                        context.getFutureResult(0).cancel(true);
                    }
                    else
                        interactionResult = TaskListener.InteractionResult.NEXT;
                    break;
                case 0:
                    interactionResult = configurePlugins();
                    break;
                case 1:
                    interactionResult = configureChain();
                    break;
                case 2:
                    interactionResult = specifyMarking();
                    break;
                case 3:
                    interactionResult = configureDecomposerSettings(interactionResult, composedModel);
                    break;
                case 4:
                    interactionResult = configureRepairerSettings(interactionResult);
                    break;
                case 5:
                    interactionResult = configureClassMapping();
                    break;
                case 6:
                    interactionResult = configureReplayer(false);
                    break;
                case 7:
                    interactionResult = configureMetrics();
                    break;
            }

            switch (interactionResult) {
                case NEXT:
                    screenNumber++;
                    break;
                case PREV:
                    screenNumber--;
                    break;
                case CANCEL:
                    //todo из-за null пром кидает исключение
                    return null;
                case FINISHED:
                    ConformanceCheckingUsingAlignments conformanceChecker =
                            new ConformanceCheckingUsingAlignments(algorithmStep.getAlgorithm(), replayParameter);

                    IskraFinalEvaluation evaluator = new IskraFinalEvaluationImpl(metrics, conformanceChecker);

                    RepairChain chain = createRepairChain(conformanceChecker, evaluator);

                    if (chain == null) {
                        screenNumber = 1;
                        break;
                    }

                    composedModel.getNet().setInitialMarking(initialMarking);
                    Set<Marking> finalMarkings = new HashSet<Marking>();

                    finalMarkings.add(finalMarking);
                    composedModel.getNet().setFinalMarkings(finalMarkings);
                    composedModel.setMapping(mapping);

                    IskraResult result =
                            chain.execute(context, composedModel, mapping);

                    /*ExportIskraResultAsCSVPlugin exportPlugin = new ExportIskraResultAsCSVPlugin();
                    try {
                        exportPlugin.exportToConstantFile(result);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }   */

                    return new Object[]{result, result.getFinalModel().getNet()};
            }
        }
    }

    private RepairChain createRepairChain(ConformanceCheckingUsingAlignments conformanceChecker, IskraFinalEvaluation evaluator) {

        Class<RepairChain> selectedChainClass = namesToChainClasses.get(selectedChainName);

        try {
            Constructor<RepairChain> constructor = selectedChainClass.getConstructor(
                    IskraDecomposer.class, IskraRepairer.class,
                    IskraFinalEvaluation.class, ConformanceCheckingUsingAlignments.class, Metrics.class, double.class);

            return constructor.newInstance(
                    decomposer, repairer,
                    evaluator, conformanceChecker,
                    metrics, fitnessThreshold);

        } catch (Exception e) {
            e.printStackTrace();

            JOptionPane.showMessageDialog(
                    null, "Selected repair chain doesn't work properly. Select another chain.",
                    "Error", JOptionPane.ERROR_MESSAGE);

            return null;
        }

    }

    private TaskListener.InteractionResult configureChain() {

        if (namesToChainClasses == null) {
            loadChains();
        }

        SingleChoicePanel<String> chainSelectionPanel = new SingleChoicePanel<String>(
                namesToChainClasses.keySet(), selectedChainName);

        TaskListener.InteractionResult interactionResult;

        do {
            interactionResult = context.showWizard("Select one repair chain", false, false, chainSelectionPanel);
            selectedChainName = chainSelectionPanel.getChosenOption();

            if (selectedChainName == null && interactionResult != TaskListener.InteractionResult.CANCEL) {
                JOptionPane.showMessageDialog(null, "You have to choose one repair chain");
            }

        } while (interactionResult != TaskListener.InteractionResult.CANCEL && selectedChainName == null);

        return interactionResult;
    }

    @SuppressWarnings("unchecked")
    private void loadChains() {
        namesToChainClasses = new HashMap<>();

        PluginManager pluginManager = context.getPluginManager();

        Set<Class<?>> chainClasses = pluginManager.getKnownClassesAnnotatedWith(Chain.class);

        for (Class<?> chainClass : chainClasses) {
            if (!chainClass.isInterface() && RepairChain.class.isAssignableFrom(chainClass)) {
                if (!Modifier.isAbstract(chainClass.getModifiers())) {
                    Chain chainAnnotation = chainClass.getAnnotation(Chain.class);
                    String chainName = chainAnnotation.value();

                    namesToChainClasses.put(chainName, (Class<RepairChain>) chainClass);
                }
            }
        }
    }

    private boolean precheck(){
        Set<String> labels = new HashSet<>();
        for(Transition transition : this.petrinet.getTransitions()) {
            String label = transition.getAttributeMap().get(AttributeMap.LABEL).toString();
            label = label.replaceAll("complete|\\+", "");
            labels.add(label);
        }
        int transitionSize = labels.size();

        for(Object obj : log.getClassifiers()) {
            System.out.println(obj.getClass() + ":\t" + obj);
        }

        for(int i = 0; i < log.size(); i++){
            XTrace xTrace = log.get(i);
            for(int j = 0; j < xTrace.size(); j++){
                XEvent xEvent = xTrace.get(j);
                labels.add(xEvent.getAttributes().get("concept:name").toString());
                if(labels.size() > transitionSize) {
                    int res = JOptionPane.showConfirmDialog(null, "Fitness is too low. Continue?", "Warning", JOptionPane.OK_CANCEL_OPTION);
                    return res == JOptionPane.OK_OPTION;
                }
            }
        }

        return true;

    }

    private TaskListener.InteractionResult configurePlugins() {

        if (allRepiarers == null || allDecomposers == null) {
            loadPlugins();
        }

        IskraSettingsPanel iskraSettingsPanel = new IskraSettingsPanel(
                decomposer, repairer,
                allDecomposers, allRepiarers,
                fitnessThreshold);

        TaskListener.InteractionResult result = TaskListener.InteractionResult.CANCEL;

        boolean isSelectionSuccessful = false;

        while (!isSelectionSuccessful) {
            result = context.showWizard("Repairer and decomposer selection", true, false, iskraSettingsPanel);
            switch (result) {
                case PREV:
                case CANCEL:
                    return result;
            }
            isSelectionSuccessful = iskraSettingsPanel.verify();
        }

        decomposer = iskraSettingsPanel.getChosenDecompositionWrap();
        repairer = iskraSettingsPanel.getChosenMinerPlugin();
        fitnessThreshold = iskraSettingsPanel.getFitnessThreshold();

        return result;
    }

    private void loadPlugins() {
        allRepiarers = load(Repairer.class, IskraRepairer.class);
        allDecomposers = load(Decomposer.class, IskraDecomposer.class);
    }

    @SuppressWarnings("unchecked")
    private <T> List<T> load(Class<? extends Annotation> annotation, Class<T> objectClass) {
        List<T> instantiatedPlugins = new ArrayList<>();

        PluginManager pluginManager = context.getPluginManager();

        Set<Class<?>> pluginClasses = pluginManager.getKnownClassesAnnotatedWith(annotation);

        for (Class<?> pluginClass : pluginClasses) {

            if (objectClass.isAssignableFrom(pluginClass)) {

                if (!Modifier.isAbstract(pluginClass.getModifiers())) {
                    try {
                        T object = (T) pluginClass.newInstance();

                        instantiatedPlugins.add(object);
                    } catch (InstantiationException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return instantiatedPlugins;
    }
}
