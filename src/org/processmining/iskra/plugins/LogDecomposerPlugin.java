package org.processmining.iskra.plugins;

import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.iskra.utils.LogDecomposer;
import org.processmining.log.models.EventLogArray;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;

/**
 * Created by Ivan on 05.05.2015.
 */
public class LogDecomposerPlugin {

    @Plugin(
            name = "Project Decomposed Model on Event Log",
            returnLabels = {"Event Log Parts Corresponding to the Net Parts"},
            returnTypes = {EventLogArray.class},
            parameterLabels = {"Accepting Petri net Array", "Event Log"}
    )
    @UITopiaVariant(
            affiliation = "HSE PAIS Lab",
            author = "Alex Mitsyuk, Ivan Shugurov",
            email = "amitsyuk@hse.ru")
    public EventLogArray decompose(PluginContext context, AcceptingPetriNetArray netArray, XLog log, TransEvClassMapping mapping) {
        LogDecomposer decomposer = new LogDecomposer();
        return decomposer.decompose(netArray, log, mapping);
    }
}
