package org.processmining.iskra.plugins.steps;

import nl.tue.astar.AStarException;
import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.iskra.infrastucture.impl.BadPartsWrapper;
import org.processmining.iskra.infrastucture.impl.WrappingResult;
import org.processmining.iskra.pluginwraps.conformance.DecomposedConformanceChecker;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.iskra.types.DecomposedConformanceResult;
import org.processmining.iskra.types.ImpossibilityToMeasureConformanceException;
import org.processmining.iskra.types.Metrics;
import org.processmining.iskra.types.impl.DecomposedModelImpl;
import org.processmining.iskra.utils.PlusRemoval;
import org.processmining.log.models.EventLogArray;
import org.processmining.log.models.impl.EventLogArrayFactory;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

import java.util.ArrayList;
import java.util.List;

import static org.processmining.iskra.utils.DanglingTransitionsRemover.removeDanglingTransitions;
import static org.processmining.iskra.utils.LogProjection.projectNetOnLog;

/**
 * Created by Ivan on 18.04.2016.
 */
public class AnotherMiningStep extends NaiveMiningStep {

    private DecomposedConformanceChecker decomposedConformanceChecker;

    @Plugin(name = "Step by step ISKRA: repair bad parts (another)", returnLabels = "AcceptingPetriNet", returnTypes = AcceptingPetriNetArray.class, parameterLabels = {"Accepting net", "Log"})
    @UITopiaVariant(affiliation = "HSE", email = "", author = "")
    @Override
    public AcceptingPetriNetArray repairBadParts(UIPluginContext context, AcceptingPetriNetArray netArray, XLog log) throws ImpossibilityToMeasureConformanceException, AStarException {
        return super.repairBadParts(context, netArray, log, false);

    }

    @Override
    protected AcceptingPetriNetArray repair(AcceptingPetriNetArray netArray, EventLogArray decompsedLog, double fitnessThreshold, IskraRepairer repairer, PluginContext context) throws AStarException, ImpossibilityToMeasureConformanceException {
        decomposedConformanceChecker = new DecomposedConformanceChecker(new Metrics(), conformanceChecker, fitnessThreshold);

        DecomposedConformanceResult decomposedConformanceResult = decomposedConformanceChecker.measureDecomposedConformance(context, netArray, decompsedLog, mapping, true);

        List<AcceptingPetriNet> badParts = new ArrayList<>();

        for (int i = 0; i < netArray.getSize(); i++) {
            if (decomposedConformanceResult.isBadPart(i)) {
                badParts.add(netArray.getNet(i));
            }
        }

        BadPartsWrapper wrapper = new BadPartsWrapper();
        WrappingResult wrappingResult = wrapper.wrapBadParts(context, netArray, badParts);
        AcceptingPetriNetArray wrappers = wrappingResult.getBadParts();

        EventLogArray projectedLogs = EventLogArrayFactory.createEventLogArray();

        for (int i = 0; i < wrappers.getSize(); i++) {
            AcceptingPetriNet acceptingNet = wrappers.getNet(i);
            Petrinet petrinet = acceptingNet.getNet();
            XLog projection = projectNetOnLog(petrinet, this.log);
            projectedLogs.addLog(projection);
        }

        DecomposedConformanceResult conformanceResultOfWrappers = decomposedConformanceChecker.measureDecomposedConformance(context, new DecomposedModelImpl(wrappers, projectedLogs), mapping, true);

        int indexOfWorstPart = -1;
        double worstConformance = 2;
        for (int i = 0; i < wrappers.getSize(); i++) {
            if (worstConformance > conformanceResultOfWrappers.getConformance(i)) {
                indexOfWorstPart = i;
                worstConformance = conformanceResultOfWrappers.getConformance(i);
            }
        }

        if (indexOfWorstPart < 0) {
            return null;
        }

        System.out.println("Initial marking of wrapper: " + wrappers.getNet(indexOfWorstPart).getInitialMarking());
        System.out.println("Final marking of wrapper: " + wrappers.getNet(indexOfWorstPart).getFinalMarkings());


        Petrinet repairedNet = repairer.repair(context, projectedLogs.getLog(indexOfWorstPart));
        repairedNet = PlusRemoval.removePluses(repairedNet);
        removeInitialFinalPlaces(repairedNet);
        removeDanglingTransitions(repairedNet);
        wrappers.removeNet(indexOfWorstPart);


        AcceptingPetriNet repairedAcceptingNet = AcceptingPetriNetFactory.createAcceptingPetriNet(repairedNet);

        AcceptingPetriNetArray netParts = wrappingResult.getOtherParts();
        netParts.addNet(repairedAcceptingNet);

        for (int i = 0; i < wrappers.getSize(); i++) {
            netParts.addNet(wrappers.getNet(i));
        }

        return netParts;
    }
}
