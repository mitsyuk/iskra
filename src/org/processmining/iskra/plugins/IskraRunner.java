package org.processmining.iskra.plugins;

import org.deckfour.uitopia.api.event.TaskListener;
import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.classification.XEventClasses;
import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.connections.ConnectionCannotBeObtained;
import org.processmining.framework.connections.ConnectionManager;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.iskra.dialogs.MetricsPanel;
import org.processmining.iskra.infrastucture.AbstractIskraRepairChainArray;
import org.processmining.iskra.types.Metrics;
import org.processmining.iskra.infrastucture.impl.IskraRepairChainImpl;
import org.processmining.iskra.pluginwraps.IskraFinalEvaluationImpl;
import org.processmining.iskra.types.AbstractIskraCombinedResult;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.IskraResult;
import org.processmining.iskra.types.impl.ComposedModelImpl;
import org.processmining.models.connections.petrinets.behavioral.FinalMarkingConnection;
import org.processmining.models.connections.petrinets.behavioral.InitialMarkingConnection;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.semantics.petrinet.Marking;
import org.processmining.plugins.connectionfactories.logpetrinet.MappingPanel;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;
import org.processmining.plugins.petrinet.replayer.algorithms.IPNReplayParamProvider;
import org.processmining.plugins.petrinet.replayer.algorithms.IPNReplayParameter;
import org.processmining.plugins.petrinet.replayer.ui.PNAlgorithmStep;
import ru.hse.pais.shugurov.widgets.panels.MultipleChoicePanel;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by user on 10.07.14.
 */
@Plugin(name = "Iskra chain runner", parameterLabels = {"Net", "Log", "Chain", "Chain pack"},
        returnLabels = {"Iskra testing result"}, returnTypes = {IskraResult.class})
public class IskraRunner {
    private static final double DEFAULT_FITNESS_THRESHOLD = 0.75;
    int i = 0;
    private UIPluginContext context;
    private Petrinet petrinet;
    private XLog log;
    private TransEvClassMapping mapping;
    private int mappingStep = 0;
    private Marking initialMarking;
    private Marking finalMarking;
    private int replayerConfigurationStep = 0;
    private PNAlgorithmStep algorithmStep;
    private IPNReplayParameter replayParameter;
    private IskraFinalEvaluationImpl evaluator;
    private Metrics metrics = new Metrics();

    @UITopiaVariant(affiliation = "HSE PAIS Lab",
            author = "Alexey Mitsyuk, Ivan Shugurov, Dmitry Yakovlev",
            email = "amitsyuk" + (char) 0x40 + "hse.ru",
            pack = "DecomposedReplay")
    @PluginVariant(variantLabel = "Iskra chain runner, UI", requiredParameterLabels = {0, 1, 2})
    public IskraResult run(UIPluginContext context,
                           Petrinet petrinet,
                           XLog log, IskraRepairChainImpl chain) throws Exception {
        this.context = context;
        this.petrinet = petrinet;
        this.log = log;
        int screenNumber = 0;
        tryToFindMarkingConnections();
        TaskListener.InteractionResult interactionResult = TaskListener.InteractionResult.CANCEL;

        ComposedModel composedModel = new ComposedModelImpl(petrinet, log);

        while (true) {
            switch (screenNumber) {
                case 0:
                    interactionResult = createMarkingConnections();
                    break;
                case 1:
                    interactionResult = configureClassMapping();
                    break;
                case 2:
                    interactionResult = configureReplayer();
                    break;
                case 3:
                    interactionResult = configureMetrics();
                    break;
            }
            switch (interactionResult) {
                case NEXT:
                    screenNumber++;
                    break;
                case PREV:
                    screenNumber--;
                    break;
                case CANCEL:
                    return null;
                case FINISHED:
                    return chain.execute(context, composedModel, mapping);
            }
        }
    }

    @UITopiaVariant(affiliation = "HSE PAIS Lab",
            author = "Alexey Mitsyuk, Ivan Shugurov, Dmitry Yakovlev",
            email = "amitsyuk" + (char) 0x40 + "hse.ru",
            pack = "DecomposedReplay")
    @PluginVariant(variantLabel = "Iskra pack runner, UI", requiredParameterLabels = {0, 1, 3})
    public AbstractIskraCombinedResult run(UIPluginContext context,
                                           Petrinet petrinet,
                                           XLog log, AbstractIskraRepairChainArray chainArray) throws Exception {
        this.context = context;
        this.petrinet = petrinet;
        this.log = log;
        int screenNumber = 0;
        tryToFindMarkingConnections();
        TaskListener.InteractionResult interactionResult = TaskListener.InteractionResult.CANCEL;

        ComposedModel composedModel = new ComposedModelImpl(petrinet, log);

        while (true) {
            switch (screenNumber) {
                case 0:
                    interactionResult = createMarkingConnections();
                    break;
                case 1:
                    interactionResult = configureClassMapping();
                    break;
                case 2:
                    interactionResult = configureReplayer();
                    break;
                case 3:
                    interactionResult = configureMetrics();
                    break;
            }
            switch (interactionResult) {
                case NEXT:
                    screenNumber++;
                    break;
                case PREV:
                    screenNumber--;
                    break;
                case CANCEL:
                    return null;
                case FINISHED:
                    return chainArray.execute(context, composedModel, 0, mapping);
            }
        }
    }

    private void tryToFindMarkingConnections() {
        ConnectionManager connectionManager = context.getConnectionManager();
        try {
            InitialMarkingConnection initialMarkingConnection = connectionManager.getFirstConnection(InitialMarkingConnection.class, context, petrinet);
            initialMarking = initialMarkingConnection.getObjectWithRole(InitialMarkingConnection.MARKING);
        } catch (ConnectionCannotBeObtained connectionCannotBeObtained) {
            initialMarking = new Marking();
        }
        try {
            FinalMarkingConnection finalMarkingConnection = connectionManager.getFirstConnection(FinalMarkingConnection.class, context, petrinet);
            finalMarking = finalMarkingConnection.getObjectWithRole(FinalMarkingConnection.MARKING);
        } catch (ConnectionCannotBeObtained connectionCannotBeObtained) {
            finalMarking = new Marking();
        }
        initialMarking.minus(finalMarking);
        finalMarking.minus(initialMarking);
    }

    private TaskListener.InteractionResult createMarkingConnections() {
        if (mappingStep < 0) {
            mappingStep = 0;
        }
        if (mappingStep > 1) {
            mappingStep = 1;
        }
        TaskListener.InteractionResult interactionResult = TaskListener.InteractionResult.CANCEL;
        while (true) {
            switch (mappingStep) {
                case 0:
                    interactionResult = createInitialMarkingConnection();
                    break;
                case 1:
                    interactionResult = createFinalMarkingConnection();
                    break;
                default:
                    return interactionResult;
            }
            switch (interactionResult) {
                case PREV:
                    mappingStep--;
                    break;
                case NEXT:
                    mappingStep++;
                    break;
                default:
                    return interactionResult;
            }
        }
    }

    private TaskListener.InteractionResult createInitialMarkingConnection() {
        Collection<Place> possibleInitialPlaces = new HashSet<Place>();
        possibleInitialPlaces.addAll(petrinet.getPlaces());
        possibleInitialPlaces.removeAll(finalMarking.baseSet());
        MultipleChoicePanel<Place> markingPanel = new MultipleChoicePanel<Place>(possibleInitialPlaces, initialMarking.baseSet());
        while (true) {
            TaskListener.InteractionResult interactionResult = context.showWizard("Initial marking", true, false, markingPanel);
            switch (interactionResult) {
                case NEXT:
                    initialMarking = new Marking(markingPanel.getChosenOptionsAsSet());
                    if (initialMarking.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "You have to choose at least one initial place", "Error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        InitialMarkingConnection initialMarkingConnection = new InitialMarkingConnection(petrinet, initialMarking);
                        context.addConnection(initialMarkingConnection);
                        return interactionResult;
                    }
                    break;
                case PREV:
                    initialMarking = new Marking(markingPanel.getChosenOptionsAsSet());
                default:
                    return interactionResult;
            }
        }
    }

    private TaskListener.InteractionResult createFinalMarkingConnection() {
        Collection<Place> possibleFinalPlaces = new HashSet<Place>();
        possibleFinalPlaces.addAll(petrinet.getPlaces());
        possibleFinalPlaces.removeAll(initialMarking.baseSet());
        MultipleChoicePanel<Place> markingPanel = new MultipleChoicePanel<Place>(possibleFinalPlaces, finalMarking.baseSet());
        while (true) {
            TaskListener.InteractionResult interactionResult = context.showWizard("Final marking", false, false, markingPanel);
            switch (interactionResult) {
                case NEXT:
                    finalMarking = new Marking(markingPanel.getChosenOptionsAsSet());
                    if (finalMarking.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "You have to choose at least one final place", "Error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        FinalMarkingConnection finalMarkingConnection = new FinalMarkingConnection(petrinet, finalMarking);
                        context.addConnection(finalMarkingConnection);
                        return interactionResult;
                    }
                    break;
                case PREV:
                    finalMarking = new Marking(markingPanel.getChosenOptionsAsSet());
                default:
                    return interactionResult;
            }
        }
    }

    private TaskListener.InteractionResult configureClassMapping() {
        TaskListener.InteractionResult interactionResult;
        List<XEventClassifier> classList = new ArrayList<XEventClassifier>(log.getClassifiers());
        if (!classList.contains(XLogInfoImpl.RESOURCE_CLASSIFIER)) {
            classList.add(0, XLogInfoImpl.RESOURCE_CLASSIFIER);
        }
        if (!classList.contains(XLogInfoImpl.NAME_CLASSIFIER)) {
            classList.add(0, XLogInfoImpl.NAME_CLASSIFIER);
        }
        if (!classList.contains(XLogInfoImpl.STANDARD_CLASSIFIER)) {
            classList.add(0, XLogInfoImpl.STANDARD_CLASSIFIER);
        }
        MappingPanel connectionFactoryUI = new MappingPanel(log, petrinet, classList.toArray());
        interactionResult = context.showWizard("Mapping Petrinet - Event Class of Log", false, false, connectionFactoryUI);
        if (interactionResult == TaskListener.InteractionResult.NEXT || interactionResult == TaskListener.InteractionResult.FINISHED) {
            XLogInfo summary = XLogInfoFactory.createLogInfo(log, connectionFactoryUI.getSelectedClassifier());
            XEventClasses eventClasses = summary.getEventClasses();
            Collection<XEventClass> colEventClasses = new HashSet<XEventClass>(eventClasses.getClasses());
            colEventClasses.removeAll(connectionFactoryUI.getMap().values());
        }
        XLogInfo summary = XLogInfoFactory.createLogInfo(log, connectionFactoryUI.getSelectedClassifier());
        XEventClasses eventClasses = summary.getEventClasses();
        Collection<XEventClass> colEventClasses = new HashSet<XEventClass>(eventClasses.getClasses());
        colEventClasses.removeAll(connectionFactoryUI.getMap().values());
        return interactionResult;
    }

    private TaskListener.InteractionResult configureReplayer() {
        if (replayerConfigurationStep < 0) {
            replayerConfigurationStep = 0;
        }
        if (replayerConfigurationStep > 1) {
            replayerConfigurationStep = 1;
        }
        TaskListener.InteractionResult result = TaskListener.InteractionResult.CANCEL;
        while (true) {
            switch (replayerConfigurationStep) {
                case 0:
                    System.out.println("<Aria>");
                    if (algorithmStep == null) {
                        algorithmStep = new PNAlgorithmStep(context, petrinet, log, mapping);
                    }
                    System.out.println("</Aria>");
                    result = context.showWizard("Replay in Petrinet", false, false, algorithmStep);
                    break;
                case 1:
                    IPNReplayParamProvider paramProvider = algorithmStep.getAlgorithm().constructParamProvider(context, petrinet, log, mapping);
                    JComponent paramsComponent = paramProvider.constructUI();
                    result = context.showWizard("Replay in Petrinet", false, false, paramsComponent);
                    replayParameter = paramProvider.constructReplayParameter(paramsComponent);
                    break;
                default:
                    return result;
            }
            switch (result) {
                case NEXT:
                    replayerConfigurationStep++;
                    break;
                case PREV:
                    replayerConfigurationStep--;
                    break;
                default:
                    return result;
            }
        }
    }

    private TaskListener.InteractionResult configureMetrics() {
        boolean isInputCorrect = false;

        MetricsPanel metricsPanel = new MetricsPanel(metrics);
        TaskListener.InteractionResult interactionResult = TaskListener.InteractionResult.CANCEL;

        while (!isInputCorrect) {
            interactionResult = context.showWizard("Metrics settings", false, true, metricsPanel);
            isInputCorrect = metricsPanel.verify();
            switch (interactionResult) {
                case NEXT:
                case FINISHED:
                    if (!isInputCorrect) {
                        JOptionPane.showMessageDialog(null, "Incorrect input occurs", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
                default:
                    return interactionResult;
            }
        }

        return interactionResult;
    }

}
