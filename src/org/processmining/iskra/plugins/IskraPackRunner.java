package org.processmining.iskra.plugins;

import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.iskra.types.IskraResult;

/**
 * Created by user on 10.07.14.
 */
@Plugin(name = "Iskra pack runner", parameterLabels = {},
        returnLabels = {"Iskra repair chain"}, returnTypes = {IskraResult.class})
public class IskraPackRunner {

    @UITopiaVariant(affiliation = "HSE PAIS Lab",
            author = "Alexey Mitsyuk, Ivan Shugurov, Dmitry Yakovlev",
            email = "amitsyuk" + (char) 0x40 + "hse.ru",
            pack = "DecomposedReplay")
    @PluginVariant(variantLabel = "Iskra chain runner, UI", requiredParameterLabels = {})
    public void run() {

    }
}
