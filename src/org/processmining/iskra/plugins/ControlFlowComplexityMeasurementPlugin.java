package org.processmining.iskra.plugins;

import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.iskra.utils.ControlFlowComplexityMeasurer;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

/**
 * Created by Ivan on 14.07.2015.
 */
public class ControlFlowComplexityMeasurementPlugin {
    //TODO int?
    @Plugin(name = "Measure control-flow complexity", returnLabels = "Complexity", returnTypes = Integer.class, parameterLabels = "Petri net")
    @UITopiaVariant(affiliation = "HSE PAIS Lab", email = "amitsyuk@hse.ru", author = "Alex Mitsyuk, Ivan Shugurov")
    public int measureComplexity(PluginContext context, Petrinet petrinet) {
        ControlFlowComplexityMeasurer measurer = new ControlFlowComplexityMeasurer();
        return measurer.measure(petrinet);
    }
}
