package org.processmining.iskra.pluginwraps.conformance;

/**
 * Created by Ivan on 16.07.2015.
 */
public class Conformance {
    private double fitness;
    private double precision;
    private double generalization;

    public Conformance(double fitness, double precision, double generalization) {
        this.fitness = fitness;
        this.precision = precision;
        this.generalization = generalization;
    }

    public double getFitness() {
        return fitness;
    }

    public double getPrecision() {
        return precision;
    }

    public double getGeneralization() {
        return generalization;
    }
}
