package org.processmining.iskra.pluginwraps.conformance;

import nl.tue.astar.AStarException;
import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.infrastucture.impl.DecomposedConformanceResultImpl;
import org.processmining.iskra.types.*;
import org.processmining.iskra.types.impl.DecomposedModelImpl;
import org.processmining.iskra.utils.PlusRemoval;
import org.processmining.log.models.EventLogArray;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;
import org.processmining.processtree.Event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ivan on 18.04.2016.
 */
public class DecomposedConformanceChecker {

    ConformanceCheckingUsingAlignments conformanceChecker;
    double fitnessThreshold;
    private Metrics metrics;

    public DecomposedConformanceChecker(Metrics metrics, ConformanceCheckingUsingAlignments conformanceChecker, double fitnessThreshold) {
        this.metrics = metrics;
        this.conformanceChecker = conformanceChecker;
        this.fitnessThreshold = fitnessThreshold;
    }

    public DecomposedConformanceResult measureDecomposedConformance(PluginContext context, DecomposedModel model, TransEvClassMapping mapping, boolean printResults) throws AStarException, ImpossibilityToMeasureConformanceException {
        List<Double> conformanceList = new ArrayList<>();

        AcceptingPetriNetArray netArray = model.getNetArray();
        EventLogArray logArray = model.getLogArray();

        HashMap<String, XEventClass> labelToEventMap = new HashMap<>();
        HashMap<XEventClass, Transition> eventToTransitionMap = new HashMap<>();
        for(Map.Entry<Transition, XEventClass> pair : mapping.entrySet()) {
            labelToEventMap.put(pair.getKey().getAttributeMap().get(AttributeMap.LABEL).toString(), pair.getValue());
            eventToTransitionMap.put(pair.getValue(), pair.getKey());
        }

        for (int i = 0; i < netArray.getSize(); i++) {
            AcceptingPetriNet acceptingNet = netArray.getNet(i);
            XLog log = logArray.getLog(i);

            TransEvClassMapping decomposedMapping = new TransEvClassMapping(mapping.getEventClassifier(), mapping.getDummyEventClass());
            for(Transition transition : acceptingNet.getNet().getTransitions())
                decomposedMapping.put(transition, labelToEventMap.get(PlusRemoval.removeLastPlus(transition.getAttributeMap().get(AttributeMap.LABEL).toString())));

            double conformance = measureConformanceOfNewNet(context, acceptingNet, log, decomposedMapping);

            if (printResults) {
                System.out.println("Net " + i + " conformance: " + conformance);
            }

            conformanceList.add(conformance);
        }

        return new DecomposedConformanceResultImpl(conformanceList, fitnessThreshold);
    }

    public DecomposedConformanceResult measureDecomposedConformance(PluginContext context, AcceptingPetriNetArray netArray,
                                                                    EventLogArray logArray, TransEvClassMapping mapping, boolean printResults) throws ImpossibilityToMeasureConformanceException, AStarException {
        DecomposedModel model = new DecomposedModelImpl(netArray, logArray);
        return measureDecomposedConformance(context, model, mapping, printResults);
    }

    protected double measureConformanceOfNewNet(PluginContext context, AcceptingPetriNet petriNet, XLog log, TransEvClassMapping mapping) throws AStarException, ImpossibilityToMeasureConformanceException {
        double evaluatedConformance;

        if (metrics.measureOnlyFitness()) {
            evaluatedConformance = conformanceChecker.measureFitnessOfNewNet(context, petriNet, log, mapping);
        } else {
            Conformance conformance = conformanceChecker.measureConformanceOfNewNet(context, petriNet, log, mapping);
            evaluatedConformance = metrics.evaluateInitialModel(conformance);
        }

        return evaluatedConformance;
    }

    public double getFitnessThreshold() {
        return fitnessThreshold;
    }
}