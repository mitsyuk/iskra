package org.processmining.iskra.pluginwraps.conformance;

import nl.tue.astar.AStarException;
import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.contexts.uitopia.UIContext;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.util.Pair;
import org.processmining.iskra.plugins.IskraBase;
import org.processmining.iskra.types.ImpossibilityToMeasureConformanceException;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetGraph;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.semantics.petrinet.Marking;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;
import org.processmining.plugins.petrinet.replayer.PNLogReplayer;
import org.processmining.plugins.petrinet.replayer.algorithms.IPNReplayAlgorithm;
import org.processmining.plugins.petrinet.replayer.algorithms.IPNReplayParameter;
import org.processmining.plugins.petrinet.replayer.algorithms.swapping.CostBasedSwapParam;
import org.processmining.plugins.petrinet.replayresult.PNRepResult;
import org.processmining.plugins.pnalignanalysis.conformance.AlignmentPrecGen;
import org.processmining.plugins.pnalignanalysis.conformance.AlignmentPrecGenRes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Ivan on 16.07.2015.
 */
public class ConformanceCheckingUsingAlignments {

    private IPNReplayAlgorithm algorithm;
    private IPNReplayParameter parameter;


    public ConformanceCheckingUsingAlignments(IPNReplayAlgorithm algorithm, IPNReplayParameter parameter) {
        this.algorithm = algorithm;
        this.parameter = parameter;
    }

    public double measureFitness(PluginContext context, Petrinet petrinet, XLog log, TransEvClassMapping mapping) throws AStarException, ImpossibilityToMeasureConformanceException {
        return measureFitness(context, petrinet, log, mapping, parameter);
    }

    public double measureFitnessOfNewNet(PluginContext context, AcceptingPetriNet petrinet, XLog log, TransEvClassMapping mapping) throws AStarException, ImpossibilityToMeasureConformanceException {
        IPNReplayParameter parameterCopy = adjustParameterToNewModel(petrinet);
        return measureFitness(context, petrinet.getNet(), log, mapping, parameterCopy);
    }

    /**
     * measures fitness, precision, generalization
     *
     * @return
     */
    public Conformance measureConformance(PluginContext context, Petrinet petrinet, XLog log, TransEvClassMapping mapping,
                                          Marking initialMarking) throws AStarException, ImpossibilityToMeasureConformanceException {
        return measureConformance(context, petrinet, log, mapping, parameter, initialMarking);
    }

    private Conformance measureConformance(PluginContext context, Petrinet petrinet, XLog log,TransEvClassMapping mapping,
                                           IPNReplayParameter parameter, Marking initialMarking) throws AStarException, ImpossibilityToMeasureConformanceException {
        Conformance conformance = null;

        PNRepResult replayResult = (new PNLogReplayer()).replayLog(context, petrinet, log, mapping, algorithm, parameter);

        boolean correctReplay = checkCorrectness(replayResult);

        if (correctReplay) {

            if (canMeasurePrecisionAndGeneralization(petrinet)) {

                AlignmentPrecGenRes precgenResult = measurePrecisionAndGeneralization(context, mapping, replayResult, petrinet, initialMarking);

                double finalPrecision = precgenResult.getPrecision();
                double finalGeneralization = precgenResult.getGeneralization();

                conformance = new Conformance(getFitness(replayResult), finalPrecision, finalGeneralization);
            } else {
                indicateImpossibilityOfMeasuringPrecisionGeneralization();
            }

        } else {
            indicateImpossibilityOfMeasuringFitness();
        }

        return conformance;
    }

    /**
     * measures fitness, precision, generalization
     *
     * @return
     */
    public Conformance measureConformanceOfNewNet(PluginContext context, AcceptingPetriNet petrinet, XLog log, TransEvClassMapping mapping) throws AStarException, ImpossibilityToMeasureConformanceException {
        IPNReplayParameter parameter = adjustParameterToNewModel(petrinet);
        return measureConformance(context, petrinet.getNet(), log, mapping, parameter, petrinet.getInitialMarking());
    }

    public boolean canMeasurePrecisionAndGeneralization(Petrinet petrinet) {
        for (Transition transition : petrinet.getTransitions()) {
            if (transition.isInvisible() && petrinet.getOutEdges(transition).isEmpty()) {
                return false;
            }
        }

        return true;
    }

    public boolean canMeasurePrecisionAndGeneralization(AcceptingPetriNet petrinet) {
        return canMeasurePrecisionAndGeneralization(petrinet.getNet());
    }

    private AlignmentPrecGenRes measurePrecisionAndGeneralization(PluginContext context,
                                                                  TransEvClassMapping mapping, PNRepResult alignment, Petrinet net, Marking initMarking) {
        //TODO ����� ������...
        AlignmentPrecGen alignmentPrecGen = new AlignmentPrecGen();
        return alignmentPrecGen.measureConformanceAssumingCorrectAlignment(context, mapping, alignment, net, initMarking, true);
    }

    private double measureFitness(PluginContext context, Petrinet petrinet, XLog log, TransEvClassMapping mapping, IPNReplayParameter replayParameter) throws AStarException, ImpossibilityToMeasureConformanceException {

        PNRepResult replayResult = (new PNLogReplayer()).replayLog(context, petrinet, log, mapping, algorithm, replayParameter);

        double fitness = getFitness(replayResult);
        boolean correctReplay = checkCorrectness(replayResult);

        // TODO: why is the replay incorrect?
        return fitness;
//        if (correctReplay) {
//            return fitness;
//        } else {
//            indicateImpossibilityOfMeasuringFitness();
//            return 0;
//        }
    }

    private boolean checkCorrectness(PNRepResult replayResult) {
        double traceLength = (Double) replayResult.getInfo().get(PNRepResult.ORIGTRACELENGTH);
        return traceLength != 0;
    }

    private Double getFitness(PNRepResult replayResult) {
        return (Double) replayResult.getInfo().get(PNRepResult.TRACEFITNESS);
    }

    private IPNReplayParameter adjustParameterToNewModel(AcceptingPetriNet petrinet) {
        IPNReplayParameter parameterCopy = parameter.copyParameter(petrinet.getNet());
        parameterCopy.setInitialMarking(petrinet.getInitialMarking());

        Set<Marking> finalMarkings = petrinet.getFinalMarkings();

        if (!finalMarkings.isEmpty()) {
            parameterCopy.setFinalMarkings(finalMarkings.toArray(new Marking[finalMarkings.size()]));
        }
        return parameterCopy;
    }

    private void indicateImpossibilityOfMeasuringPrecisionGeneralization() throws ImpossibilityToMeasureConformanceException {
        throw new ImpossibilityToMeasureConformanceException(
                "Impossible to measure precision/generalization due to " +
                        "the existence of silent transitions without outgoing arcs");
    }

    private void indicateImpossibilityOfMeasuringFitness() throws ImpossibilityToMeasureConformanceException {
        throw new ImpossibilityToMeasureConformanceException(
                "Impossible to measure fitness");
    }
}
