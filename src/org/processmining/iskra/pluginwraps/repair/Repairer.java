package org.processmining.iskra.pluginwraps.repair;

import java.lang.annotation.*;

/**
 * Created by Ivan on 26.03.2015.
 */
@Target(ElementType.TYPE)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface Repairer {
}
