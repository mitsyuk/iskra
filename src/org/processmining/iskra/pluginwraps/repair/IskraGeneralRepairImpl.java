package org.processmining.iskra.pluginwraps.repair;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.util.Pair;
import org.processmining.iskra.pluginwraps.AbstractIskraPlugin;
import org.processmining.iskra.types.DecomposedConformanceResult;
import org.processmining.iskra.types.DecomposedModel;
import org.processmining.iskra.types.RepairedDecomposedModel;
import org.processmining.iskra.types.impl.RepairedDecomposedModelImpl;
import org.processmining.log.models.EventLogArray;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;

import java.util.Collection;

import static org.processmining.iskra.infrastucture.RepairChain.USE_PROVIDED_OBJECTS;
import static org.processmining.iskra.utils.BoundryPlacesFinder.checkPresenceOfBoundaryPlaces;
import static org.processmining.iskra.utils.RemovalOfBoundaryPlaces.removeBoundaryPlaces;

/**
 * The basic repair algorithm for Iskra
 *
 * @author Alexey Mistyuk
 */
public class IskraGeneralRepairImpl extends AbstractIskraPlugin implements IskraGeneralRepair {

    private IskraRepairer repairer;

    public IskraGeneralRepairImpl(IskraRepairer repairer) {
        super(repairer.getPluginName());
        this.repairer = repairer;

    }

    public RepairedDecomposedModel repair(PluginContext context,
                                          DecomposedModel model,
                                          DecomposedConformanceResult conformanceResult) {

        AcceptingPetriNetArray netArray = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();
        netArray.init();
        EventLogArray logArray = model.getLogArray();

        int numberOfBadParts = 0;
        int totalSizeOfBadFragment = 0;

        for (int i = 0; i < model.getNetArray().getSize(); i++) {

            if (conformanceResult.isBadPart(i)) {

                numberOfBadParts++;
                Petrinet fragment = model.getNetArray().getNet(i).getNet();
                totalSizeOfBadFragment += fragment.getNodes().size();

                Pair<Boolean, Boolean> presenceOfBoundaryPlaces = checkPresenceOfBoundaryPlaces(fragment);
                boolean containsInitialPlaces = presenceOfBoundaryPlaces.getFirst();
                boolean containsFinalPlaces = presenceOfBoundaryPlaces.getSecond();

                Petrinet minedNet = repairer.repair(context, logArray.getLog(i));
                if (minedNet == null) {
                    return null;
                } else {
                    removeBoundaryPlaces(minedNet, containsInitialPlaces, containsFinalPlaces);

                    org.processmining.acceptingpetrinet.models.AcceptingPetriNet acceptingNet = AcceptingPetriNetFactory.createAcceptingPetriNet(minedNet);
                    netArray.addNet(acceptingNet);

                    if (USE_PROVIDED_OBJECTS) {
                        System.out.println("Remined part " + i);
                        System.out.println("Fitness: " + conformanceResult.getConformance(i));
                    }
                }

            } else {
                if (USE_PROVIDED_OBJECTS) {
                    System.out.println("Took as it was " + i);
                    System.out.println("Fitness: " + conformanceResult.getConformance(i));
                }

                netArray.addNet(model.getNetArray().getNet(i));
            }
        }

        System.out.println("Total number of fragments: " + model.getNetArray().getSize());
        System.out.println("Number of bad fragments: " + numberOfBadParts);
        System.out.println("Total size of bad fragments: " + totalSizeOfBadFragment);

        return new RepairedDecomposedModelImpl(netArray, model.getLogArray(), netArray);
    }

    private void removeFinalPlaces(Petrinet petrinet) {
        boolean finished;
        do {
            finished = true;
            for (Place place : petrinet.getPlaces()) {
                Collection outEdges = petrinet.getOutEdges(place);
                if (outEdges.isEmpty()) {
                    finished = false;
                    petrinet.removePlace(place);
                    break;
                }
            }
        } while (!finished);
    }

    private void removeInitialPlace(Petrinet petrinet) {
        boolean finished;
        do {
            finished = true;
            for (Place place : petrinet.getPlaces()) {
                Collection inEdges = petrinet.getInEdges(place);
                if (inEdges.isEmpty()) {
                    finished = false;
                    petrinet.removePlace(place);
                    break;
                }
            }
        } while (!finished);
    }

    @Override
    public String getComment() {
        return repairer.getComment();
    }
}
