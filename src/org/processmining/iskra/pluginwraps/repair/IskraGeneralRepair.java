package org.processmining.iskra.pluginwraps.repair;

import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.types.DecomposedConformanceResult;
import org.processmining.iskra.types.DecomposedModel;
import org.processmining.iskra.types.RepairedDecomposedModel;

/**
 * The basic idea of repair
 *
 * @author Alexey Mitsyuk
 */
public interface IskraGeneralRepair {

    /**
     * Repair provided model
     *
     * @param context           Prom 6 plugin context
     * @param model             model to perform repairing on
     * @param conformanceResult conformance results
     * @return performed repair result
     */
    RepairedDecomposedModel repair(PluginContext context,
                                   DecomposedModel model,
                                   DecomposedConformanceResult conformanceResult);

    /**
     * Some comment associated with repair object
     *
     * @return comment from used repair
     */
    String getComment();

    String getPluginName();

}
