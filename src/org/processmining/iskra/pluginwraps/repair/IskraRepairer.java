package org.processmining.iskra.pluginwraps.repair;

import org.deckfour.xes.model.XLog;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.pluginwraps.AbstractIskraPlugin;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

import javax.swing.*;

/**
 * The base for iskra repair
 *
 * @author Ivan Shugurov
 */
@Repairer
public abstract class IskraRepairer extends AbstractIskraPlugin {

    /**
     * Basic constructor for defining plugin name
     *
     * @param pluginName the name of concrete plugin
     */
    protected IskraRepairer(String pluginName) {
        super(pluginName);
    }

    /**
     * The main method
     *
     * @param context Prom 6 plugin context
     * @param log     Log to mine from
     * @return Petri net from log
     */
    public abstract Petrinet repair(PluginContext context, XLog log);

    /**
     * Settings panel for the plugin
     *
     * @return settings panel for the plugin
     */
    public JComponent getSettingsComponent() {
        return null;
    }

    /**
     * Some unknown and empty method
     */
    public abstract void saveSettings();

    /**
     * Super mega overrider for comment!!
     *
     * @return ""
     */
    public abstract String getComment();
}