package org.processmining.iskra.pluginwraps.repair.impl;

import org.deckfour.xes.model.XLog;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.KeepInProMCache;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.plugins.InductiveMiner.mining.MiningParameters;
import org.processmining.plugins.InductiveMiner.mining.MiningParametersIMi;
import org.processmining.plugins.InductiveMiner.plugins.IMPetriNet;

import javax.swing.*;

import static org.processmining.iskra.types.PetrinetHelper.addPluses;

/**
 * Wrapper for Inductive miner
 *
 * @author Ivan Shugurov
 */
@KeepInProMCache
public class InductiveRepairer extends IskraRepairer { //TODO не умею показывать настройки майнера
    private MiningParameters parameters = new MiningParametersIMi();

    public InductiveRepairer() {
        super("Inductive miner");
        parameters.setNoiseThreshold(0);
    }

    @Override
    public Petrinet repair(PluginContext context, XLog log) {

        //long start = System.currentTimeMillis();
        Object[] results = IMPetriNet.minePetriNet(context, log, parameters);
        //long end = System.currentTimeMillis();

        //System.out.println("time:" +(end - start));

        Petrinet petrinet = (Petrinet) results[0];
        return petrinet;
//        return addPluses(petrinet);
    }

    @Override
    public JComponent getSettingsComponent() {
        /*miningDialog = new IMMiningDialog(null);
        return miningDialog;  TODO */
        return null;
    }

    @Override
    public void saveSettings() {

    }

    @Override
    public String getComment() {
        return "";
    }
}
