package org.processmining.iskra.pluginwraps.repair.impl;

import org.deckfour.xes.model.XLog;
import org.processmining.framework.connections.Connection;
import org.processmining.framework.connections.ConnectionCannotBeObtained;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.KeepInProMCache;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.iskra.pluginwraps.repair.Repairer;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

/**
 * Wrapper for Alpha-miner
 *
 * @author Ivan Shugurov
 */
@Repairer
@KeepInProMCache
public class AlphaRepairer extends IskraRepairer {
    public AlphaRepairer() {
        super("Alpha miner");
    }

    @Override
    public Petrinet repair(PluginContext context, XLog log) {
        try {
            return context.tryToFindOrConstructFirstNamedObject(Petrinet.class, "Alpha Miner", Connection.class, "", log);
        } catch (ConnectionCannotBeObtained connectionCannotBeObtained) {
            connectionCannotBeObtained.printStackTrace();
            return null;
        }
    }

    @Override
    public void saveSettings() {
    }

    @Override
    public String getComment() {
        return "";
    }
}
