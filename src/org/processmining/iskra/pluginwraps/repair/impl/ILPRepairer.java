package org.processmining.iskra.pluginwraps.repair.impl;

import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XLog;
import org.processmining.framework.connections.ConnectionCannotBeObtained;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.KeepInProMCache;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.iskra.utils.PlusRemoval;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.plugins.ilpminer.ILPMinerSettings;
import org.processmining.plugins.ilpminer.ILPMinerUI;

import javax.swing.*;

/**
 * Wrapper for ILP miner
 *
 * @author Ivan Shugurov
 */
@KeepInProMCache
public class ILPRepairer extends IskraRepairer {
    private ILPMinerSettings settings = new ILPMinerSettings();
    private ILPMinerUI ilpMinerUI;

    public ILPRepairer() {
        super("ILP Miner");
    }

    @Override
    public Petrinet repair(PluginContext context, XLog log) {
        XLogInfo logInfo = XLogInfoFactory.createLogInfo(log);
        Petrinet minedNet = null;
        try {
           // long start = System.currentTimeMillis();
            minedNet = context.tryToFindOrConstructFirstNamedObject(Petrinet.class, "ILP Miner", null, null, log, logInfo, settings);
            //long end = System.currentTimeMillis();
            //System.out.println("time: " + (end - start));
        } catch (ConnectionCannotBeObtained connectionCannotBeObtained) {
            connectionCannotBeObtained.printStackTrace();
        }
        return PlusRemoval.removePluses(minedNet);
    }

    @Override
    public JComponent getSettingsComponent() {
        ilpMinerUI = new ILPMinerUI(settings);
        return ilpMinerUI.initComponents();
    }

    @Override
    public void saveSettings() {
        if (ilpMinerUI != null) {
            settings = ilpMinerUI.getSettings();
        }
    }

    @Override
    public String getComment() {
        return "Solver settings: " + settings.getSolverSettings() + "\nModel settings: " + settings.getModelSettings() + "\nExtensions: " + settings.getExtensions() + "\n";
    }
}
