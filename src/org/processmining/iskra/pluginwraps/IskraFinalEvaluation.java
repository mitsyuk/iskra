package org.processmining.iskra.pluginwraps;

import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.FinalEvaluationResult;
import org.processmining.iskra.types.IskraTimingResultBuilder;

/**
 * The basic idea of final evaluation
 *
 * @author Alexey Mitsyuk
 */
public interface IskraFinalEvaluation {

    /**
     * Do final evaluation!
     *
     * @param context             Prom 6 plugin context
     * @param initialModel        initial model + log
     * @param finalModel          final model + log
     * @param timingResultBuilder
     * @return evaluation result
     */
    FinalEvaluationResult evaluate(PluginContext context,
                                   ComposedModel initialModel,
                                   ComposedModel finalModel,
                                   IskraTimingResultBuilder timingResultBuilder,
                                   String chainName);

}
