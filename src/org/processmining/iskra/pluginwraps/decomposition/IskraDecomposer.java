package org.processmining.iskra.pluginwraps.decomposition;

import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.acceptingpetrinet.parameters.MergeAcceptingPetriNetArrayIntoAcceptingPetriNetParameters;
import org.processmining.acceptingpetrinet.plugins.MergeAcceptingPetriNetArrayIntoAcceptingPetriNetPlugin;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.pluginwraps.AbstractIskraPlugin;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.DecomposedModel;
import org.processmining.iskra.types.PetrinetHelper;
import org.processmining.iskra.types.impl.ComposedModelImpl;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetEdge;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;
import org.processmining.models.semantics.petrinet.Marking;

import javax.swing.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Alexey Mitsyuk
 */
@Decomposer
public abstract class IskraDecomposer extends AbstractIskraPlugin {

    protected IskraDecomposer(String pluginName) {
        super(pluginName);
    }

    protected AcceptingPetriNet copyNetAddingPluses(Petrinet petrinet) {   //TODO почему возвращаемые типы разные, не указаны маркинги?
        Petrinet copy = PetrinetHelper.addPluses(petrinet);
        return AcceptingPetriNetFactory.createAcceptingPetriNet(copy);
    }

    protected Petrinet copyRemovingPluses(Petrinet petrinet) {   //TODO почему возвращаемые типы разные?
         Petrinet copy = PetrinetFactory.clonePetrinet(petrinet);

        for (Transition transition : petrinet.getTransitions()){
             String label = transition.getLabel();

            if (label.endsWith("+"))
            {
                  for (Transition transitionToBeRenamed : copy.getTransitions()){
                      if (transitionToBeRenamed.getLabel().equals(label))
                      {
                          Collection<PetrinetEdge<?, ?>> inEdges = copy.getInEdges(transitionToBeRenamed);
                          Collection<PetrinetEdge<?, ?>> outEdges = copy.getOutEdges(transitionToBeRenamed);

                          copy.removeTransition(transitionToBeRenamed);

                          String labelWithoutPlus = label.substring(0, label.indexOf('+'));

                          Transition renamedTransition = copy.addTransition(labelWithoutPlus);

                          for (PetrinetEdge inEdge : inEdges){
                              Place precedingPlace =  (Place) inEdge.getSource();
                              copy.addArc(precedingPlace, renamedTransition);
                          }

                          for (PetrinetEdge outEdge : outEdges){
                              Place succeedingPlace = (Place) outEdge.getTarget();
                              copy.addArc(renamedTransition, succeedingPlace);
                          }

                          break;
                      }
                  }
            }
        }

        return copy;
    }

    public abstract DecomposedModel decompose(PluginContext context, ComposedModel initialModel);

    public ComposedModel compose(PluginContext context, DecomposedModel decomposedModel, XLog initialLog) {   //TODO а что делает метод compose в классе decomposer
        AcceptingPetriNet mergedNet =
                new MergeAcceptingPetriNetArrayIntoAcceptingPetriNetPlugin().run(
                        context, decomposedModel.getNetArray(), new MergeAcceptingPetriNetArrayIntoAcceptingPetriNetParameters());

        Petrinet mergedPetriNet = mergedNet.getNet();

        boolean initialPlaceFound = false;
        for (Place initialPlaceCandidate : mergedPetriNet.getPlaces()) {
            Collection inEdges = mergedPetriNet.getInEdges(initialPlaceCandidate);
            if (inEdges.isEmpty()) {
                initialPlaceFound = true;
                Marking initialMarking = new Marking();
                initialMarking.add(initialPlaceCandidate);
                mergedNet.setInitialMarking(initialMarking);
                break;
            }
        }
        if (!initialPlaceFound) {
            Place initialPlace = mergedPetriNet.addPlace("begin");
            for (Transition transition : mergedPetriNet.getTransitions()) {
                Collection inEdges = mergedPetriNet.getInEdges(transition);
                if (inEdges.isEmpty()) {
                    mergedPetriNet.addArc(initialPlace, transition);
                }
            }
            Marking initialMarking = new Marking();
            initialMarking.add(initialPlace);
            mergedNet.setInitialMarking(initialMarking);
        }

        boolean finalPlaceFound = false;
        for (Place finalPlaceCandidate : mergedPetriNet.getPlaces()) {
            Collection outEdges = mergedPetriNet.getOutEdges(finalPlaceCandidate);
            if (outEdges.isEmpty()) {
                finalPlaceFound = true;
                Marking finalMarking = new Marking();
                finalMarking.add(finalPlaceCandidate);
                Set<Marking> finalMarkingSet = new HashSet<Marking>();
                finalMarkingSet.add(finalMarking);
                mergedNet.setFinalMarkings(finalMarkingSet);
                break;
            }
        }
        if (!finalPlaceFound) {
            Place finalPlace = mergedPetriNet.addPlace("end");
            for (Transition transition : mergedPetriNet.getTransitions()) {
                Collection outEdges = mergedPetriNet.getOutEdges(transition);
                if (outEdges.isEmpty()) {
                    mergedPetriNet.addArc(transition, finalPlace);
                }
            }
            Marking finalMarking = new Marking();
            finalMarking.add(finalPlace);
            Set<Marking> finalMarkingSet = new HashSet<Marking>();
            finalMarkingSet.add(finalMarking);
            mergedNet.setFinalMarkings(finalMarkingSet);
        }

        return new ComposedModelImpl(mergedNet.getNet(), initialLog);
    }

    public JComponent getSettingsComponent() {
        return null;
    }

    public String getComment() {
        return "";
    }
}