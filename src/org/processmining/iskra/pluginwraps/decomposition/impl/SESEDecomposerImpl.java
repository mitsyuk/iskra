package org.processmining.iskra.pluginwraps.decomposition.impl;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.activityclusterarray.models.ActivityClusterArray;
import org.processmining.activityclusterarrayextractor.plugins.ExtractActivityClusterArrayFromAcceptingPetriNetArrayPlugin;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.KeepInProMCache;
import org.processmining.iskra.dialogs.SESEDecomposerSettingsPanel;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.DecomposedModel;
import org.processmining.iskra.types.impl.DecomposedModelImpl;
import org.processmining.log.models.EventLogArray;
import org.processmining.logdecomposer.parameters.DecomposeEventLogUsingActivityClusterArrayParameters;
import org.processmining.logdecomposer.plugins.DecomposeEventLogUsingActivityClusterArrayPlugin;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.impl.DivideAndConquerFactory;
import org.processmining.parameters.dc.DecomposeBySESEsAndBridgingParameters;
import org.processmining.plugins.dc.DecomposeBySESEsAndBridgingPlugin;

import javax.swing.*;
import java.util.Collection;

/**
 * @author Dmitriy &lt;Zimy(x)&gt; Yakovlev
 */
@KeepInProMCache
public class SESEDecomposerImpl extends IskraDecomposer {
    private final DecomposeBySESEsAndBridgingParameters parameters = new DecomposeBySESEsAndBridgingParameters();
    private boolean settingsChanged = false;

    public SESEDecomposerImpl() {
        super("SESE Decomposer");
    }

    @Override
    public DecomposedModel decompose(PluginContext context, ComposedModel initialModel) {
        AcceptingPetriNet initialAcceptingNet = initialModel.getNet();
        Petrinet petrinet = initialAcceptingNet.getNet();
        Collection edges = petrinet.getEdges();
        int modelSize = edges.size();

        if (settingsChanged) {
            parameters.setMaxSize((parameters.getMaxSize() * modelSize / 100));
        } else {
            parameters.setMaxSize((int) (modelSize * 0.3));
            settingsChanged = true;
        }

        AcceptingPetriNet acceptingPetriNet = copyNetAddingPluses(petrinet);

        DecomposeBySESEsAndBridgingPlugin sesePlugin = new DecomposeBySESEsAndBridgingPlugin();
        AcceptingPetriNetArray decomposition =
                convertDeprecatedAcceptingNetArrayToAcceptingNetArray(
                        sesePlugin.decomposeParameters(context, convertAcceptingNetToDeprecatedAcceptingNet(acceptingPetriNet), parameters));

        ActivityClusterArray activityClusterArray = new ExtractActivityClusterArrayFromAcceptingPetriNetArrayPlugin().runDefault(context, decomposition);

        DecomposeEventLogUsingActivityClusterArrayParameters parameters = new DecomposeEventLogUsingActivityClusterArrayParameters(
                initialModel.getLog());
        parameters.setRemoveEmptyTraces(true);
        parameters.setAddStartEndEvents(false);
        EventLogArray eventLogArray = new DecomposeEventLogUsingActivityClusterArrayPlugin().run(context, initialModel.getLog(), activityClusterArray, parameters);


        return new DecomposedModelImpl(decomposition, eventLogArray);  //TODO в библиотеке ecomposedConformance он использует устаревший класс AcceptingPetriNet
    }

    @Override
    public JComponent getSettingsComponent() {
        settingsChanged = true;
        return new SESEDecomposerSettingsPanel(parameters);
    }

    @SuppressWarnings("deprecation")
    private org.processmining.models.AcceptingPetriNet convertAcceptingNetToDeprecatedAcceptingNet(AcceptingPetriNet acceptingPetriNet) {
        org.processmining.models.AcceptingPetriNet deprecatedNet = DivideAndConquerFactory.createAcceptingPetriNet();
        deprecatedNet.init(acceptingPetriNet.getNet());
        deprecatedNet.setInitialMarking(acceptingPetriNet.getInitialMarking());
        deprecatedNet.setFinalMarkings(acceptingPetriNet.getFinalMarkings());

        return deprecatedNet;
    }

    @SuppressWarnings("deprecation")
    private AcceptingPetriNet convertDeprecatedAcceptingNetToAcceptingNet(org.processmining.models.AcceptingPetriNet deprecatedNet) {
        AcceptingPetriNet acceptingPetriNet = AcceptingPetriNetFactory.createAcceptingPetriNet();
        acceptingPetriNet.init(deprecatedNet.getNet());
        acceptingPetriNet.setInitialMarking(deprecatedNet.getInitialMarking());
        acceptingPetriNet.setFinalMarkings(deprecatedNet.getFinalMarkings());

        return acceptingPetriNet;
    }

    @SuppressWarnings("deprecation")
    private AcceptingPetriNetArray convertDeprecatedAcceptingNetArrayToAcceptingNetArray(org.processmining.models.AcceptingPetriNetArray deprecatedArray) {
        AcceptingPetriNetArray acceptingPetriNetArray = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

        for (int i = 0; i < deprecatedArray.getSize(); i++) {
            acceptingPetriNetArray.addNet(convertDeprecatedAcceptingNetToAcceptingNet(deprecatedArray.getNet(i)));
        }

        return acceptingPetriNetArray;
    }

}
