package org.processmining.iskra.pluginwraps.decomposition.impl;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinetdecomposer.plugins.DecomposeAcceptingPetriNetUsingActivityClusterArrayPlugin;
import org.processmining.activityclusterarray.models.ActivityClusterArray;
import org.processmining.activityclusterarraycreator.plugins.ConvertCausalActivityGraphToActivityClusterArrayPlugin;
import org.processmining.causalactivitygraph.models.CausalActivityGraph;
import org.processmining.causalactivitygraphcreator.plugins.ConvertCausalActivityMatrixToCausalActivityGraphPlugin;
import org.processmining.causalactivitymatrix.models.CausalActivityMatrix;
import org.processmining.causalactivitymatrixcreator.plugins.CreateFromAcceptingPetriNetPlugin;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.KeepInProMCache;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.DecomposedModel;
import org.processmining.iskra.types.impl.DecomposedModelImpl;
import org.processmining.iskra.utils.LogDecomposer;
import org.processmining.log.models.EventLogArray;
import org.processmining.logdecomposer.parameters.DecomposeEventLogUsingActivityClusterArrayParameters;
import org.processmining.logdecomposer.plugins.DecomposeEventLogUsingActivityClusterArrayPlugin;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

//import static org.processmining.iskra.infrastucture.AbstractRepairChain.copyNetAddingPluses;

/**
 * Created by Ivan on 03.07.2015.
 */

@KeepInProMCache
public class OriginalMaximalDecomposerImpl extends IskraDecomposer
 {    //TODO ����� �������� ������������� � ��������� ���������

    public OriginalMaximalDecomposerImpl() {
        super("Original maximal decomposer");
    }

    @Override
    public DecomposedModel decompose(PluginContext context, ComposedModel initialModel) {
        Petrinet initialPetriNet = initialModel.getNet().getNet();
        AcceptingPetriNet acceptingPetriNet = copyNetAddingPluses(initialPetriNet);

        org.processmining.causalactivitymatrixcreator.plugins.CreateFromAcceptingPetriNetPlugin plugin = new CreateFromAcceptingPetriNetPlugin();
        CausalActivityMatrix activityMatrix = plugin.runDefault(context, acceptingPetriNet);
        ConvertCausalActivityMatrixToCausalActivityGraphPlugin causalActivityGraphPlugin = new ConvertCausalActivityMatrixToCausalActivityGraphPlugin();
        CausalActivityGraph causalActivityGraph = causalActivityGraphPlugin.runDefault(context, activityMatrix);
        org.processmining.activityclusterarraycreator.plugins.ConvertCausalActivityGraphToActivityClusterArrayPlugin clusterArrayPlugin = new ConvertCausalActivityGraphToActivityClusterArrayPlugin();
        ActivityClusterArray activityClusterArray = clusterArrayPlugin.runDefault(context, causalActivityGraph);

        AcceptingPetriNetArray decomposition = new DecomposeAcceptingPetriNetUsingActivityClusterArrayPlugin().runDefault(context, acceptingPetriNet, activityClusterArray);

//        DecomposeEventLogUsingActivityClusterArrayPlugin logDecomposePlugin = new DecomposeEventLogUsingActivityClusterArrayPlugin();
//
//        DecomposeEventLogUsingActivityClusterArrayParameters parameters = new DecomposeEventLogUsingActivityClusterArrayParameters(initialModel.getLog());
//        parameters.setAddStartEndEvents(false);
//        parameters.setRemoveEmptyTraces(true);
//        EventLogArray eventLogArray = logDecomposePlugin.run(context, initialModel.getLog(), activityClusterArray, parameters);

        EventLogArray eventLogArray = (new LogDecomposer()).decompose(decomposition, initialModel.getLog(), initialModel.getMapping());

        return new DecomposedModelImpl(decomposition, eventLogArray);
    }
}
