package org.processmining.iskra.pluginwraps.decomposition.impl;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayImpl;
import org.processmining.acceptingpetrinet.parameters.MergeAcceptingPetriNetArrayIntoAcceptingPetriNetParameters;
import org.processmining.acceptingpetrinet.plugins.MergeAcceptingPetriNetArrayIntoAcceptingPetriNetPlugin;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.KeepInProMCache;
import org.processmining.iskra.pluginwraps.conformance.DecomposedConformanceChecker;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.DecomposedConformanceResult;
import org.processmining.iskra.types.DecomposedModel;
import org.processmining.iskra.types.impl.DecomposedModelImpl;
import org.processmining.iskra.utils.LogDecomposer;
import org.processmining.log.models.EventLogArray;
import org.processmining.log.models.impl.EventLogArrayImpl;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetNode;
import org.processmining.models.graphbased.directed.petrinet.elements.Arc;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.plugins.modelCompare.Pair;

import java.util.*;

class Graph {

    private Node root;
    private ArrayList<Node> allNodes;

    Graph() {
        allNodes = new ArrayList<>();
    }

    void addRoot(Node root) {
        this.root = root;
        addNodeToList(root);
    }

    Node findNode(int value) {
        for (Node n : allNodes)
            if (n.getValue() == value)
                return n;

        return null;
    }

    void addNodeToList(Node nd) {
        Node foundNode = findNode(nd.getValue());
        if (foundNode == null)
            allNodes.add(nd);
        else
            nd.assignNode(foundNode);
    }

    ArrayList<Node> getAllNodes() {
        return allNodes;
    }
}

class Node {
    private ArrayList<Node> children;
    //    private ArrayList<Node> parents;
    private int value;
    private Graph graph;

    Node(int value, Graph graph) {
        this.value = value;
        this.graph = graph;

        children = new ArrayList<>();
//        parents = new ArrayList<>();
        graph.addNodeToList(this);
    }

    public boolean equals(Object o) {
        if (o instanceof Node)
            return ((Node) o).value == this.value;

        return false;
    }

    public int hashCode() {
        return value;
    }

    void addChild(Node child) {
        children.add(child);
//        child.parents.add(this);
    }

    int getValue() {
        return value;
    }

    int getNumberOfChildren() {
        return children.size();
    }

    Node getChild(int num) {
        if (children != null && children.size() > num)
            return children.get(num);
        else
            return null;
    }

//    ArrayList<Node> getParents() {
//        return parents;
//    }

    ArrayList<Node> getChildren() {
        return children;
    }

    void assignNode(Node nd) {
        this.children = nd.children;
        this.value = nd.value;
        this.graph = nd.graph;
    }

    Graph getGraph() {
        return graph;
    }
}

@KeepInProMCache
public class SmartDecomposerImpl extends IskraDecomposer {

    private DecomposedConformanceChecker conformanceChecker;

    public SmartDecomposerImpl() {
        super("Smart decomposer");
    }

    @Override
    public DecomposedModel decompose(PluginContext context, ComposedModel initialModel) {

        System.out.println("--- Starting Smart decomposer ---");
        long start = System.currentTimeMillis();

        System.out.println("SmartDecomposer: getting maximal decomposition");
        DecomposedModel maximalDecomposition = new OurMaximalDecomposerImpl().decompose(context, initialModel);

        System.out.println("SmartDecomposer: measuring maximal decomposition fitness");
        DecomposedConformanceResult conformance = null;
        try {
            conformance = conformanceChecker.measureDecomposedConformance(context, maximalDecomposition, initialModel.getMapping(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AcceptingPetriNetArray netArray = maximalDecomposition.getNetArray();
        List<Pair<Integer, AcceptingPetriNet>> badParts = new ArrayList<>();
        List<AcceptingPetriNet> allFragments = new ArrayList<>();

        double fitnessThreshold = conformanceChecker == null ? 1 : conformanceChecker.getFitnessThreshold();

        if (conformance != null)
            for (int i = 0; i < conformance.size(); i++) {
                allFragments.add(netArray.getNet(i));
                if (conformance.getConformance(i) < fitnessThreshold)
                    badParts.add(new Pair<>(i, netArray.getNet(i)));
            }

        // САМ АЛГОРИТМ
        System.out.println("SmartDecomposer: starting algorithm");

        if (badParts.size() == 0) {
            AcceptingPetriNetArray net = new AcceptingPetriNetArrayImpl();
            net.addNet(initialModel.getNet());

            EventLogArray log = new EventLogArrayImpl();
            log.addLog(initialModel.getLog());

            return new DecomposedModelImpl(net, log);
        }
        // Сначала выбрать вершину Source - пока случайным образом
        int rnd = new Random().nextInt(badParts.size());
        int sourceIndex = badParts.get(rnd).getFirst();
        AcceptingPetriNet sourceFragment = badParts.get(rnd).getSecond();
        badParts.remove(rnd);

        System.out.println("SmartDecomposer: getting graph adjacency matrix");
        // g - матрица смежности графа
        int[][] g = getAdjacencyMatrix(allFragments);

        System.out.println("SmartDecomposer: finding dominators");
        Set<Integer> dominators = new HashSet<>();

        // Находим кратчайшие пути из Source в i-ю плохую вершину
        // и добавляем в список доминаторов получившегося графа
        for (int i = 0; i < badParts.size(); i++) {

            int verticleIndex = badParts.get(i).getFirst();

            Graph pathGraph = getPath(g, sourceIndex, verticleIndex);

            dominators.addAll(findDominators(pathGraph, sourceIndex, verticleIndex));
        }

        System.out.println("SmartDecomposer: getting dominators connecting graph");
        Set<Integer> fittingFragmentsIndexes = getVerticlesOfDominatorsConnectingGraph(g, dominators);

        System.out.println("SmartDecomposer: creating decomposed model");
        return getDecomposedModel(context, maximalDecomposition, initialModel, fittingFragmentsIndexes, g);
    }

    DecomposedModel getDecomposedModel(PluginContext context, DecomposedModel maximalDecomposition, ComposedModel initialModel,
                                       Set<Integer> unfittingFragmentsIndexes, int[][] initialModelAdjacencyMatrix) {

        AcceptingPetriNetArray netArray = maximalDecomposition.getNetArray();
        AcceptingPetriNetArray fittingFragments = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray(),
                unfittingFragments = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

        Set<Integer> fittingFragmentsIndexes = new HashSet<>();
        for (int i = 0; i < initialModelAdjacencyMatrix.length; i++)
            fittingFragmentsIndexes.add(i);

        for (Integer i : unfittingFragmentsIndexes) {
            fittingFragmentsIndexes.remove(i);
            unfittingFragments.addNet(netArray.getNet(i));
        }
        for (Integer i : fittingFragmentsIndexes)
            fittingFragments.addNet(netArray.getNet(i));

        AcceptingPetriNetArray modelParts = new AcceptingPetriNetArrayImpl();

        AcceptingPetriNet fittingNet = new MergeAcceptingPetriNetArrayIntoAcceptingPetriNetPlugin().run(
                context, fittingFragments, new MergeAcceptingPetriNetArrayIntoAcceptingPetriNetParameters());
        AcceptingPetriNet unfittingNet = new MergeAcceptingPetriNetArrayIntoAcceptingPetriNetPlugin().run(
                context, unfittingFragments, new MergeAcceptingPetriNetArrayIntoAcceptingPetriNetParameters());

        modelParts.addNet(fittingNet);
        modelParts.addNet(unfittingNet);

//        boolean[] handled = new boolean[netArray.getSize()];
//
//        AcceptingPetriNetArray modelParts = new AcceptingPetriNetArrayImpl();
////        EventLogArray logParts = new EventLogArrayImpl();
//
//        Petrinet unfittingNet = new PetrinetImpl("Unfitting net"),
//                fittingNet = new PetrinetImpl("Fitting net");
//        AcceptingPetriNet unfittingFragment = new AcceptingPetriNetImpl(unfittingNet),
//                fittingFragment = new AcceptingPetriNetImpl(fittingNet);
//
//        for (int i = 0; i < netArray.getSize(); i++)
//            for (int j = i + 1; j < netArray.getSize(); j++)
//                if (dominatorsConnectingGraph[i][j] == 1) {
//                    // TODO: потом убрать комментарий и исправить свой мерджер (или вообще отказаться от него)
////                    unfittingNet = mergeNets(netArray, unfittingNet, i, j);
//                    AcceptingPetriNetArray netsToMerge = new AcceptingPetriNetArrayImpl();
//                    netsToMerge.addNet(unfittingFragment);
//                    if (PNContains(unfittingFragment.getNet().getNodes(),
//                            netArray.getNet(i).getNet().getPlaces().iterator().next()) == null)
//                        netsToMerge.addNet(netArray.getNet(i));
//                    if (PNContains(unfittingFragment.getNet().getNodes(),
//                            netArray.getNet(j).getNet().getPlaces().iterator().next()) == null)
//                        netsToMerge.addNet(netArray.getNet(j));
//                    unfittingFragment = AcceptingPetriNetMerger.mergePetriNetArray(context, netsToMerge);
//
//                    handled[i] = true;
//                    handled[j] = true;
//                }
//
//        for (int i = 0; i < netArray.getSize(); i++)
//            for (int j = i + 1; j < netArray.getSize(); j++)
//                if (initialModelAdjacencyMatrix[i][j] == 1 && !handled[i] && !handled[j])
////                    fittingNet = mergeNets(netArray, fittingNet, i, j);
//                {
//                    AcceptingPetriNetArray netsToMerge = new AcceptingPetriNetArrayImpl();
//                    netsToMerge.addNet(fittingFragment);
//                    if (PNContains(fittingFragment.getNet().getNodes(),
//                            netArray.getNet(i).getNet().getPlaces().iterator().next()) == null)
//                        netsToMerge.addNet(netArray.getNet(i));
//                    if (PNContains(fittingFragment.getNet().getNodes(),
//                            netArray.getNet(j).getNet().getPlaces().iterator().next()) == null)
//                        netsToMerge.addNet(netArray.getNet(j));
//                    fittingFragment = AcceptingPetriNetMerger.mergePetriNetArray(context, netsToMerge);
//                }
//
//        if (unfittingFragment.getNet().getNodes().size() > 0)
//            modelParts.addNet(unfittingFragment);
////        if (unfittingNet.getNodes().size() > 0) {
////            modelParts.addNet(new AcceptingPetriNetImpl(unfittingNet));
//////            logParts.addLog(LogProjection.projectNetOnLog(unfittingNet, initialModel.getLog()));
////        }
//
//        if (fittingFragment.getNet().getNodes().size() > 0)
//            modelParts.addNet(fittingFragment);
////        if (fittingNet.getNodes().size() > 0) {
////            modelParts.addNet(new AcceptingPetriNetImpl(fittingNet));
//////            logParts.addLog(LogProjection.projectNetOnLog(fittingNet, initialModel.getLog()));
////        }
        System.out.println("Number of nodes in unfitting fragment: " + fittingNet.getNet().getNodes().size());

        return new DecomposedModelImpl(modelParts, (new LogDecomposer()).decompose(modelParts, initialModel.getLog(), initialModel.getMapping()));
    }
//    Petrinet mergeNets(AcceptingPetriNetArray netArray, Petrinet fragment, int i, int j) {
//
//        Place iPlace, jPlace;
//        boolean iExisted = false, jExisted = false;
//
//        if (PNContains(fragment.getNodes(), (PetrinetNode) netArray.getNet(i).getNet().getPlaces().toArray()[0]) == null) {
//            iPlace = fragment.addPlace(((Place) netArray.getNet(i).getNet().getPlaces().toArray()[0]).getLabel());
//            iExisted = true;
//        } else
//            iPlace = (Place) netArray.getNet(i).getNet().getPlaces().toArray()[0];
//
//        if (PNContains(fragment.getNodes(), (PetrinetNode) netArray.getNet(j).getNet().getPlaces().toArray()[0]) == null) {
//            jPlace = fragment.addPlace(((Place) netArray.getNet(j).getNet().getPlaces().toArray()[0]).getLabel());
//            jExisted = true;
//        } else
//            jPlace = (Place) netArray.getNet(j).getNet().getPlaces().toArray()[0];
//
//        ArrayList<Transition> iTransitions = new ArrayList<>(),
//                jTransitions = new ArrayList<>();
//
//        Transition[] transitions;
//
//        if (iExisted) {
//            transitions = new Transition[netArray.getNet(i).getNet().getTransitions().size()];
//            netArray.getNet(i).getNet().getTransitions().toArray(transitions);
//            for (int k = 0; k < transitions.length; k++)
//                if (PNContains(fragment.getNodes(), transitions[k]) == null)
//                    iTransitions.add(fragment.addTransition((transitions[k]).getLabel()));
//                else
//                    iTransitions.add((Transition) PNContains(fragment.getNodes(), transitions[k]));
//        }
//
//        if (jExisted) {
//            transitions = new Transition[netArray.getNet(j).getNet().getTransitions().size()];
//            netArray.getNet(j).getNet().getTransitions().toArray(transitions);
//            for (int k = 0; k < transitions.length; k++)
//                if (PNContains(fragment.getNodes(), transitions[k]) == null)
//                    jTransitions.add(fragment.addTransition((transitions[k]).getLabel()));
//                else
//                    jTransitions.add((Transition) PNContains(fragment.getNodes(), transitions[k]));
//        }
//
//        if (iExisted)
//            for (int k = 0; k < iTransitions.size(); k++)
//                if (fragment.getArc(iPlace, iTransitions.get(k)) == null)
//                    fragment.addArc(iPlace, iTransitions.get(k));
//
//        if (jExisted)
//            for (int k = 0; k < jTransitions.size(); k++)
//                if (fragment.getArc(jPlace, jTransitions.get(k)) == null)
//                    fragment.addArc(jPlace, jTransitions.get(k));
//
//        return fragment;
//    }

    PetrinetNode PNContains(Collection<PetrinetNode> collection, PetrinetNode node) {

        for (PetrinetNode it : collection) {
            if (it.getClass().equals(node.getClass()) && it.getLabel().equals(node.getLabel()))
                return it;
        }

        return null;
    }

    int[][] getAdjacencyMatrix(List<AcceptingPetriNet> allFragments) {

        // Создание матрицы смежности и заполнение ее нулями
        int[][] g = new int[allFragments.size()][];
        for (int i = 0; i < allFragments.size(); i++) {
            g[i] = new int[allFragments.size()];
        }

        for (int i = 0; i < allFragments.size(); i++) {

            Petrinet currentFragment = allFragments.get(i).getNet();

            Transition[] currentFragmentTransitions = new Transition[currentFragment.getTransitions().size()];
            currentFragment.getTransitions().toArray(currentFragmentTransitions);

            Place[] currentFragmentPlaces = new Place[1];
            currentFragment.getPlaces().toArray(currentFragmentPlaces);

            List<Arc> currentFragmentArcs = new ArrayList<>();
            for (int a = 0; a < currentFragmentTransitions.length; a++) {

                if (currentFragment.getArc(currentFragmentTransitions[a], currentFragmentPlaces[0]) != null)
                    currentFragmentArcs.add(currentFragment.getArc(currentFragmentTransitions[a], currentFragmentPlaces[0]));

                if (currentFragment.getArc(currentFragmentPlaces[0], currentFragmentTransitions[a]) != null)
                    currentFragmentArcs.add(currentFragment.getArc(currentFragmentPlaces[0], currentFragmentTransitions[a]));
            }

            for (int j = i + 1; j < allFragments.size(); j++) {
                Petrinet iteratedFragment = allFragments.get(j).getNet();
                Transition[] iteratedFragmentTransitions = new Transition[iteratedFragment.getTransitions().size()];
                iteratedFragment.getTransitions().toArray(iteratedFragmentTransitions);

                Place[] iteratedFragmentPlaces = new Place[1];
                iteratedFragment.getPlaces().toArray(iteratedFragmentPlaces);

                List<Arc> iteratedFragmentArcs = new ArrayList<>();
                for (int a = 0; a < iteratedFragmentTransitions.length; a++) {

                    if (iteratedFragment.getArc(iteratedFragmentTransitions[a], iteratedFragmentPlaces[0]) != null)
                        iteratedFragmentArcs.add(iteratedFragment.getArc(iteratedFragmentTransitions[a], iteratedFragmentPlaces[0]));

                    if (iteratedFragment.getArc(iteratedFragmentPlaces[0], iteratedFragmentTransitions[a]) != null)
                        iteratedFragmentArcs.add(iteratedFragment.getArc(iteratedFragmentPlaces[0], iteratedFragmentTransitions[a]));
                }

                for (int i_cur = 0; i_cur < currentFragmentArcs.size(); i_cur++)
                    for (int i_iter = 0; i_iter < iteratedFragmentArcs.size(); i_iter++) {
                        if (currentFragmentArcs.get(i_cur).getSource().getLabel().equals(iteratedFragmentArcs.get(i_iter).getTarget().getLabel()) ||
                                currentFragmentArcs.get(i_cur).getTarget().getLabel().equals(iteratedFragmentArcs.get(i_iter).getSource().getLabel())) {
                            g[i][j] = 1;
                            g[j][i] = 1;
                        }
                    }
            }
        }

        return g;
    }

    Set<Integer> findDominators(Graph pathGraph, int sourceIndex, int verticleIndex) {

        if (pathGraph.findNode(verticleIndex) == null)
            return null;

        Map<Node, Set<Integer>> dominators = new HashMap<>();

        for (Node n : pathGraph.getAllNodes()) {

            Set<Integer> currentNodeDominators = new HashSet<>();
            if (n.getValue() == sourceIndex)
                currentNodeDominators.add(n.getValue());
            else
                for (int i = 0; i < pathGraph.getAllNodes().size(); i++)
                    currentNodeDominators.add(pathGraph.getAllNodes().get(i).getValue());

            dominators.put(n, currentNodeDominators);
        }

        boolean changed;

        do {
            changed = false;

            for (Node n : pathGraph.getAllNodes())
                changed = changed || updateDominators(dominators, n);
        }
        while (changed);

        return dominators.get(pathGraph.findNode(verticleIndex));
    }

    boolean updateDominators(Map<Node, Set<Integer>> dominators, Node n) {

        Collection<Node> successors = n.getChildren();

        Set<Integer> updatedDominators = null;

        for (Node successor : successors) {
            if (updatedDominators == null)
                updatedDominators = new HashSet<>(dominators.get(successor));
            else
                updatedDominators.retainAll(dominators.get(successor));
        }

        if (updatedDominators == null)
            updatedDominators = new HashSet<>();

        updatedDominators.add(n.getValue());

        Set<Integer> dominatorsOnPreviousStep = dominators.get(n);
        dominators.put(n, updatedDominators);

        return updatedDominators.size() != dominatorsOnPreviousStep.size();
    }

    Set<Integer> getVerticlesOfDominatorsConnectingGraph(int[][] matrix, Set<Integer> allDominators) {

        Set<Integer> verticles = new HashSet<>();

        ArrayList<Integer> uncoveredDominators = new ArrayList<>();
        for (Integer d : allDominators)
            uncoveredDominators.add(d);

        while (!uncoveredDominators.isEmpty()) {
            int from = uncoveredDominators.get(0);
            uncoveredDominators.remove(0);

            // Поиск ближайшего доминатора к выбранному
            Queue<Integer> q = new LinkedList<>();
            q.add(from);

            int[] d = new int[matrix.length], p = new int[matrix.length];

            for (int i = 0; i < d.length; i++)
                d[i] = Integer.MAX_VALUE;
            d[from] = 0;
            p[from] = -1;


            do {

                int v = q.element();
                q.remove();

                for (int j = 0; j < matrix[v].length; j++) {

                    int to = j;

                    if (matrix[v][j] == 1 && d[v] + 1 <= d[to]) {

                        if (!q.contains(to))
                            q.add(to);
                        d[to] = allDominators.contains(to) ? d[v] : d[v] + 1;
                        p[to] = v;

                        if (uncoveredDominators.contains(to)) {
                            uncoveredDominators.remove((Integer) to);

                            int i = to;
                            while (p[i] != -1) {
                                verticles.add(i);
                                verticles.add(p[i]);
//                                g[i][p[i]] = 1;
//                                g[p[i]][i] = 1;

                                i = p[i];
                            }
                        }
                    }
                }
            } while (!q.isEmpty() && !uncoveredDominators.isEmpty());

        }
        return verticles;
    }

    Graph getPath(int[][] g, int from, int destination) {

        System.out.println("SmartDecomposer: bfs implementation");

        // Реализация bfs

        Queue<Integer> q = new LinkedList<>();
        q.add(from);

        int[] d = new int[g.length];
        ArrayList<Integer>[] p = new ArrayList[g.length];

        for (int i = 0; i < d.length; i++)
            d[i] = Integer.MAX_VALUE;
        d[from] = 0;
        while (!q.isEmpty()) {

            int v = q.element();
            q.remove();

            for (int j = 0; j < g[v].length; j++) {

                int to = j;

                if (g[v][j] == 1 && d[v] + 1 <= d[to]) {

                    if (!q.contains(to))
                        q.add(to);
                    d[to] = d[v] + 1;
                    if (p[to] == null)
                        p[to] = new ArrayList<>();
                    p[to].add(v);
                }
            }
        }

        System.out.println("SmartDecomposer: getting path from v1 to v2");
        // Восстанавливаем путь из Source в i

        Node root = new Node(destination, new Graph());
        root.getGraph().addRoot(root);
        makeGraph(root, p);
        return root.getGraph();
    }

    void makeGraph(Node nd, ArrayList<Integer>[] p) {
        if (p[nd.getValue()] != null) {
            for (int i = 0; i < p[nd.getValue()].size(); i++)
                if (p[nd.getValue()].get(i) != -1) {
                    nd.addChild(new Node(p[nd.getValue()].get(i), nd.getGraph()));
                    p[nd.getValue()].remove(i);
                    p[nd.getValue()].add(i, -1);
                }

            for (int i = 0; i < nd.getNumberOfChildren(); i++)
                makeGraph(nd.getChild(i), p);
        }
    }

    public void initializeConformanceChecker(DecomposedConformanceChecker conformanceChecker) {
        this.conformanceChecker = conformanceChecker;
    }
}