package org.processmining.iskra.hammocks;

import org.deckfour.xes.model.XLog;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.util.Pair;
import org.processmining.iskra.pluginwraps.repair.impl.InductiveRepairer;
import org.processmining.iskra.utils.BoundaryPlacesFinder;
import org.processmining.iskra.utils.LogProjection;
import org.processmining.iskra.utils.PlusRemoval;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetEdge;
import org.processmining.models.graphbased.directed.petrinet.PetrinetNode;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;
import org.processmining.plugins.petrinet.replayresult.PNRepResult;
import org.processmining.plugins.petrinet.replayresult.StepTypes;
import org.processmining.plugins.replayer.replayresult.SyncReplayResult;

import java.util.*;

/**
 * Created by Ivan on 15.08.2016.
 */
public class HammockRepairer {
    public Petrinet repair(PluginContext context, Petrinet netToRepair, PNRepResult replayResult, XLog log) {
        List<Petrinet> allSortedHammock = getSortedHammocks(netToRepair);

        Set<String> labelsOfErroneousTransitions = getErroneousTransitions(replayResult);

        Petrinet minimalWrappingHammock = findMinimalWrappingHammock(allSortedHammock, labelsOfErroneousTransitions);

        if (minimalWrappingHammock == null) {
            throw new IllegalArgumentException("There are no hammock containing all the broken parts");
        }

        Set<String> getLabelOhWrappingHammock = getHammockLabels(minimalWrappingHammock);

        XLog projectedLog = LogProjection.projectNetOnLog(getLabelOhWrappingHammock, log);
        Petrinet repairedHammock = new InductiveRepairer().repair(context, projectedLog);

        Petrinet resultingNet = PetrinetFactory.clonePetrinet(netToRepair);

        Pair<List<Place>, List<Place>> hammockBoundaries = removeHammockFromNet(resultingNet, minimalWrappingHammock);
        List<Place> placesBeforeHammock = hammockBoundaries.getFirst();
        List<Place> placesAfterHammock = hammockBoundaries.getSecond();

        addHammockToNet(repairedHammock, resultingNet, placesBeforeHammock, placesAfterHammock);

        return resultingNet;
    }

    private void addHammockToNet(Petrinet repairedHammock, Petrinet resultingNet,
                                 Collection<Place> placesBeforeHammock, Collection<Place> placesAfterHammock) {
        Pair<Set<Place>, Set<Place>> boundaryPlaces = new BoundaryPlacesFinder().find(repairedHammock);
        Set<Place> initialPlaces = boundaryPlaces.getFirst();
        List<Place> initialPlacesAsList = new ArrayList<>(initialPlaces);

        Set<Place> finalPlaces = boundaryPlaces.getSecond();
        List<Place> finalPlacesAsList = new ArrayList<>(finalPlaces);

        Map<PetrinetNode, PetrinetNode> hammockNodesToNetNodes = new HashMap<>();

        for (PetrinetEdge edge : repairedHammock.getEdges()) {
            if (edge.getSource() instanceof Place) {
                // Place -> Transition
                Place source = (Place) edge.getSource();

                Transition target = (Transition) edge.getTarget();
                target = getOrCreateTransition(resultingNet, hammockNodesToNetNodes, target);

                if (!initialPlaces.contains(source)) {
                    source = getOrCreatePlace(resultingNet, hammockNodesToNetNodes, source);
                    resultingNet.addArc(source, target);
                }
            } else {
                // Transition -> Place
                Transition source = (Transition) edge.getSource();
                source = getOrCreateTransition(resultingNet, hammockNodesToNetNodes, source);

                Place target = (Place) edge.getTarget();

                if (!finalPlaces.contains(target)) {
                    target = getOrCreatePlace(resultingNet, hammockNodesToNetNodes, target);
                    resultingNet.addArc(source, target);
                }
            }
        }


        if (initialPlaces.size() == 1 && repairedHammock.getOutEdges(initialPlacesAsList.get(0)).size() == 1) {
            for (PetrinetEdge outEdge : repairedHammock.getOutEdges(initialPlacesAsList.get(0))) {
                for (Place placeBefore : placesBeforeHammock){
                    resultingNet.addArc(placeBefore, (Transition) hammockNodesToNetNodes.get(outEdge.getTarget()));
                }
            }
        } else if (initialPlaces.size() > 1) {
            Transition transitionBeforeHammock = resultingNet.addTransition("silent before hammock");
            transitionBeforeHammock.setInvisible(true);

            for (Place placeBefore : placesBeforeHammock) {
                resultingNet.addArc(placeBefore, transitionBeforeHammock);
            }

            for (Place hammockInitialPlace : initialPlaces) {
                Place place = resultingNet.addPlace(hammockInitialPlace.getLabel());
                resultingNet.addArc(transitionBeforeHammock, place);

                for (PetrinetEdge outEdge : repairedHammock.getOutEdges(hammockInitialPlace)) {
                    resultingNet.addArc(place, (Transition) hammockNodesToNetNodes.get(outEdge.getTarget()));
                }
            }
        }

        if (finalPlaces.size() == 1 && repairedHammock.getInEdges(finalPlacesAsList.get(0)).size() == 1) {
            for (PetrinetEdge inEdge : repairedHammock.getInEdges(finalPlacesAsList.get(0))) {
                for (Place placeAfter : placesAfterHammock) {
                    resultingNet.addArc((Transition) hammockNodesToNetNodes.get(inEdge.getSource()), placeAfter);
                }
            }
        } else if (finalPlaces.size() > 1) {

            Transition transitionAfterHammock = resultingNet.addTransition("silent after hammock");
            transitionAfterHammock.setInvisible(true);

            for (Place placeAfterHammock : placesAfterHammock) {
                resultingNet.addArc(transitionAfterHammock, placeAfterHammock);
            }

            for (Place hammockFinalPlace : finalPlaces) {
                Place place = resultingNet.addPlace(hammockFinalPlace.getLabel());
                resultingNet.addArc(place, transitionAfterHammock);

                for (PetrinetEdge inEdge : repairedHammock.getInEdges(hammockFinalPlace)) {
                    resultingNet.addArc((Transition) hammockNodesToNetNodes.get(inEdge.getSource()), place);
                }
            }
        }
    }

    private Transition getOrCreateTransition(Petrinet resultingNet, Map<PetrinetNode, PetrinetNode> hammockNodesToNetNodes,
                                             Transition transition) {
        if (hammockNodesToNetNodes.containsKey(transition)) {
            transition = (Transition) hammockNodesToNetNodes.get(transition);
        } else {
            Transition temp = resultingNet.addTransition(PlusRemoval.removePlus(transition.getLabel()));
            hammockNodesToNetNodes.put(transition, temp);
            transition = temp;
        }

        return transition;
    }

    private Place getOrCreatePlace(Petrinet resultingNet, Map<PetrinetNode, PetrinetNode> hammockNodesToNetNodes, Place place) {
        if (hammockNodesToNetNodes.containsKey(place)) {
            place = (Place) hammockNodesToNetNodes.get(place);
        } else {
            Place temp = resultingNet.addPlace(place.getLabel());
            hammockNodesToNetNodes.put(place, temp);
            place = temp;
        }

        return place;
    }

    private Pair<List<Place>, List<Place>> removeHammockFromNet(Petrinet resultingNet, Petrinet minimalWrappingHammock) {
        List<Place> placesBeforeHammock = new ArrayList<>();
        List<Place> placesAfterHammock = new ArrayList<>();
        Set<String> labelsToBeDeleted = new HashSet<>();

        Transition hammockStart = null;
        Transition hammockEnd = null;

        for (PetrinetNode node : minimalWrappingHammock.getNodes()) {
            labelsToBeDeleted.add(node.getLabel());

            if (minimalWrappingHammock.getInEdges(node).isEmpty()) {
                hammockStart = (Transition) node;
            }

            if (minimalWrappingHammock.getOutEdges(node).isEmpty()) {
                hammockEnd = (Transition) node;
            }
        }

        List<PetrinetNode> nodesToBeDeleted = new ArrayList<>();

        for (PetrinetNode node : resultingNet.getNodes()) {
            if (labelsToBeDeleted.contains(node.getLabel())) {
                nodesToBeDeleted.add(node);

                if (hammockStart.getLabel().equals(node.getLabel())) {
                    for (PetrinetEdge inEdge : resultingNet.getInEdges(node)) {
                        Place source = (Place) inEdge.getSource();
                        placesBeforeHammock.add(source);
                    }
                }

                if (hammockEnd.getLabel().equals(node.getLabel())) {
                    for (PetrinetEdge outEdge : resultingNet.getOutEdges(node)) {
                        Place target = (Place) outEdge.getTarget();
                        placesAfterHammock.add(target);
                    }
                }
            }
        }

        for (PetrinetNode node : nodesToBeDeleted) {
            resultingNet.removeNode(node);
        }

        return new Pair<>(placesBeforeHammock, placesAfterHammock);
    }

    private Petrinet findMinimalWrappingHammock(List<Petrinet> allSortedHammock, Set<String> labelsOfErroneousTransitions) {
        Petrinet minimalWrappingHammock = null;

        for (Petrinet hammock : allSortedHammock) {
            Set<String> hammockLabels = getHammockLabels(hammock);

            if (hammockLabels.containsAll(labelsOfErroneousTransitions)) {
                minimalWrappingHammock = hammock;
                break;
            }
        }
        return minimalWrappingHammock;
    }

    private Set<String> getErroneousTransitions(PNRepResult replayResult) {
        Set<String> labelsOfErroneousTransitions = new HashSet<>();

        for (SyncReplayResult result : replayResult) {
            for (int i = 0; i < result.getStepTypes().size(); i++) {
                StepTypes step = result.getStepTypes().get(i);

                if (step != StepTypes.LMGOOD) {
                    Object node = result.getNodeInstance().get(i);

                    if (node instanceof PetrinetNode) {
                        labelsOfErroneousTransitions.add(((PetrinetNode) node).getLabel());
                    }
                }
            }
        }
        return labelsOfErroneousTransitions;
    }

    private List<Petrinet> getSortedHammocks(Petrinet netToRepair) {
        Set<Petrinet> allHammocksAsSet = new HammocksFinder().findAll(netToRepair);
        List<Petrinet> allSortedHammock = new ArrayList<>(allHammocksAsSet);

        Collections.sort(allSortedHammock, new Comparator<Petrinet>() {
            @Override
            public int compare(Petrinet o1, Petrinet o2) {
                return o1.getTransitions().size() - o2.getTransitions().size();
            }
        });
        return allSortedHammock;
    }

    private Set<String> getHammockLabels(Petrinet hammock) {
        Set<String> hammockLabels = new HashSet<>();

        for (Transition transition : hammock.getTransitions()) {
            hammockLabels.add(transition.getLabel());
        }

        return hammockLabels;
    }
}
