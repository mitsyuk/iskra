package org.processmining.iskra.hammocks;

import org.processmining.framework.util.Pair;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetEdge;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;

import java.util.*;

/**
 * Created by Ivan on 29.07.2016.
 */
public class DominatorsFinder {
    public Dominators find(Petrinet petrinet) {
        Pair<Set<Transition>, Set<Transition>> boundaryTransitions = findBoundaryTransitions(petrinet);

        Set<Transition> initialTransitions = boundaryTransitions.getFirst();
        Set<Transition> finalTransitions = boundaryTransitions.getSecond();

        Pair<Map<Transition, Set<Transition>>, Map<Transition, Set<Transition>>> initialSearchState = initializeDominators(petrinet, initialTransitions, finalTransitions);

        Map<Transition, Set<Transition>> preDominators = initialSearchState.getFirst();
        Map<Transition, Set<Transition>> postDominators = initialSearchState.getSecond();

        boolean changed;

        do {
            changed = false;

            for (Transition transition : petrinet.getTransitions()) {
                changed = changed || updatePreDominators(preDominators, transition);
                changed = changed || updatePostDominators(postDominators, transition);
            }
        } while (changed);

        return new Dominators(postDominators, preDominators);
    }

    boolean updatePostDominators(Map<Transition, Set<Transition>> postDominators, Transition transition) {
        Collection<Transition> successors = transition.getVisibleSuccessors();

        Set<Transition> updatedPostDominators = null;

        for (Transition successor : successors) {
            if (updatedPostDominators == null) {
                updatedPostDominators = new HashSet<>(postDominators.get(successor));
            } else {
                updatedPostDominators.retainAll(postDominators.get(successor));
            }
        }

        if (updatedPostDominators == null) {
            updatedPostDominators = new HashSet<>();
        }

        updatedPostDominators.add(transition);

        Set<Transition> postDominatorsOnPreviousStep = postDominators.get(transition);
        postDominators.put(transition, updatedPostDominators);

        return updatedPostDominators.size() != postDominatorsOnPreviousStep.size();
    }

    boolean updatePreDominators(Map<Transition, Set<Transition>> preDominators, Transition transition) {
        Collection<Transition> predecessors = transition.getVisiblePredecessors();

        Set<Transition> updatedPreDominators = null;

        for (Transition predecessor : predecessors) {
            if (updatedPreDominators == null) {
                updatedPreDominators = new HashSet<>(preDominators.get(predecessor));
            } else {
                updatedPreDominators.retainAll(preDominators.get(predecessor));
            }
        }

        if (updatedPreDominators == null) {
            updatedPreDominators = new HashSet<>();
        }

        updatedPreDominators.add(transition);

        Set<Transition> preDominatorsOnPreviousStep = preDominators.get(transition);
        preDominators.put(transition, updatedPreDominators);

        return preDominatorsOnPreviousStep.size() != updatedPreDominators.size();
    }

    Pair<Set<Transition>, Set<Transition>> findBoundaryTransitions(Petrinet petrinet) {
        for (Transition transition : petrinet.getTransitions()) {
            if (petrinet.getInEdges(transition).isEmpty()) {
                throw new IllegalArgumentException("transition without incoming arcs");
            }

            if (petrinet.getOutEdges(transition).isEmpty()) {
                throw new IllegalArgumentException("transition without outgoing arcs");
            }
        }

        Set<Transition> initialTransitions = null;
        Set<Transition> finalTransitions = null;

        for (Place place : petrinet.getPlaces()) {
            Collection<PetrinetEdge<?, ?>> inEdges = petrinet.getInEdges(place);
            Collection<PetrinetEdge<?, ?>> outEdges = petrinet.getOutEdges(place);

            if (inEdges.isEmpty() && outEdges.isEmpty()) {
                throw new IllegalArgumentException("Not WF model - place without incoming and outgoing arcs");
            }

            if (inEdges.isEmpty()) {
                if (initialTransitions == null) {

                    initialTransitions = new HashSet<>();

                    for (PetrinetEdge edge : outEdges) {
                        Transition initialTransition = (Transition) edge.getTarget();
                        initialTransitions.add(initialTransition);
                    }
                } else {
                    throw new IllegalArgumentException("Not WF model - multiple places without incoming arcs ");
                }
            }

            if (outEdges.isEmpty()) {
                if (finalTransitions == null) {
                    finalTransitions = new HashSet<>();

                    for (PetrinetEdge inEdge : inEdges) {
                        Transition finalTransition = (Transition) inEdge.getSource();

                        finalTransitions.add(finalTransition);
                    }
                } else {
                    throw new IllegalArgumentException("Not WF model - multiple places without outgoing arcs ");
                }
            }
        }

        if (initialTransitions == null || finalTransitions == null) {
            throw new IllegalArgumentException("Not WF model - no initial or final place");
        }

        return new Pair<>(initialTransitions, finalTransitions);
    }

    Pair<Map<Transition, Set<Transition>>, Map<Transition, Set<Transition>>> initializeDominators(
            Petrinet petrinet, Set<Transition> initialTransitions, Set<Transition> finalTransitions) {
        Map<Transition, Set<Transition>> preDominators = new HashMap<>();
        Map<Transition, Set<Transition>> postDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            Set<Transition> preDominatorsForTransition = new HashSet<>();
            Set<Transition> postDominatorsForTransition = new HashSet<>();

            if (initialTransitions.contains(transition)) {
                preDominatorsForTransition.add(transition);
            } else {
                preDominatorsForTransition.addAll(petrinet.getTransitions());
            }

            if (finalTransitions.contains(transition)) {

                postDominatorsForTransition.add(transition);
            } else {
                postDominatorsForTransition.addAll(petrinet.getTransitions());
            }

            preDominators.put(transition, preDominatorsForTransition);
            postDominators.put(transition, postDominatorsForTransition);
        }

        return new Pair<>(preDominators, postDominators);
    }
}
