package org.processmining.iskra.hammocks;

import org.processmining.models.graphbased.directed.petrinet.Petrinet;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Ivan on 04.08.2016.
 */
public class HammockTree {
    private Petrinet originalNet;
    private Map<Petrinet, Collection<Petrinet>> layers = new HashMap<>();
    private Map<Petrinet, Set<String>> hammocksToNodeNames = new HashMap<>();
    private Set<Petrinet> allHammocks;

    public HammockTree(Petrinet originalNet) {
        this(originalNet, null);
    }

    public HammockTree(Petrinet originalNet, Set<Petrinet> allHammocks) {
        this.originalNet = originalNet;
        this.allHammocks = allHammocks;
    }

    public Map<Petrinet, Collection<Petrinet>> getLayers() {
        return layers;
    }

    public void setLayers(Map<Petrinet, Collection<Petrinet>> layers) {
        this.layers = layers;
    }

    public Petrinet getOriginalNet() {
        return originalNet;
    }

    public Map<Petrinet, Set<String>> getHammocksToNodeNames() {
        return hammocksToNodeNames;
    }

    public void setHammockToNodeNames(Petrinet hammock, Set<String> names){
        hammocksToNodeNames.put(hammock, names);
    }

    public void setHammocksToNodeNames(Map<Petrinet, Set<String>> hammocksToNodeNames) {
        this.hammocksToNodeNames = hammocksToNodeNames;
    }

    public Set<String> getNodeNames(Petrinet hammock) {
        return hammocksToNodeNames.get(hammock);
    }

    public Set<Petrinet> getAllHammocks() {
        return allHammocks;
    }

    public void setAllHammocks(Set<Petrinet> allHammocks) {
        this.allHammocks = allHammocks;
    }
}
