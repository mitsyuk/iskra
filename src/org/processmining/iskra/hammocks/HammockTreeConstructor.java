package org.processmining.iskra.hammocks;

import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetNode;

import java.util.*;

/**
 * Created by Ivan on 05.08.2016.
 */
public class HammockTreeConstructor {
    public HammockTree constructHammockTree(Petrinet petrinet) {
        return constructHammockTree(petrinet, false);
    }

    public HammockTree constructHammockTreeWithQuasiHammocks(Petrinet petrinet) {
        return constructHammockTree(petrinet, true);
    }

    public HammockTree constructHammockTree(Petrinet petrinet, boolean addQuasiHammocks) {
        Set<Petrinet> hammocks = new HammocksFinder().find(petrinet, addQuasiHammocks, true);

        HammockTree tree = new HammockTree(petrinet, new HashSet<>(hammocks));

        hammocks.add(petrinet);

        for (Petrinet hammock : hammocks) {
            Set<String> names = new HashSet<>();

            tree.setHammockToNodeNames(hammock, names);

            for (PetrinetNode node : hammock.getNodes()) {
                names.add(node.getLabel());
            }
        }

        constructHammockTree(hammocks, tree);

        return tree;
    }

    protected void constructHammockTree(Set<Petrinet> hammocks, HammockTree hammockTree) {
        List<Petrinet> sortedHammocks = getSortedHammocks(hammocks);

        Map<Petrinet, Collection<Petrinet>> hammockToChildHammocks = new HashMap<>();

        for (int i = sortedHammocks.size() - 1; i > 0; i--) {
            Petrinet currentHammock = sortedHammocks.get(i);
            Set<String> currentSetOfNames = hammockTree.getNodeNames(currentHammock);

            for (int j = i - 1; j >= 0; j--) {
                if (hammockTree.getNodeNames(sortedHammocks.get(j)).containsAll(currentSetOfNames)) {
                    //current hammock is a sub-graph of pairs.get(j)

                    Petrinet parentHammock = sortedHammocks.get(j);
                    Collection<Petrinet> hammockToSmallerHammocksOfParentHammock = hammockToChildHammocks.get(parentHammock);

                    if (hammockToSmallerHammocksOfParentHammock == null) {
                        hammockToSmallerHammocksOfParentHammock = new ArrayList<>();
                        hammockToChildHammocks.put(parentHammock, hammockToSmallerHammocksOfParentHammock);
                    }

                    hammockToSmallerHammocksOfParentHammock.add(currentHammock);

                    break;
                }
            }
        }

        hammockTree.setLayers(hammockToChildHammocks);
    }

    private List<Petrinet> getSortedHammocks(Set<Petrinet> hammocks) {
        List<Petrinet> sortedHammocks = new ArrayList<>(hammocks);

        Collections.sort(sortedHammocks, new Comparator<Petrinet>() {
            @Override
            public int compare(Petrinet o1, Petrinet o2) {
                int o1Size = o1.getTransitions().size();
                int o2Size = o2.getTransitions().size();

                if (o1Size > o2Size) {
                    return -1;
                } else if (o1Size > o2Size) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });

        return sortedHammocks;
    }
}
