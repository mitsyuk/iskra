package org.processmining.iskra.types;

/**
 * Created by Ivan on 27.07.2015.
 */
public class ImpossibilityToMeasureConformanceException extends Exception {
    public ImpossibilityToMeasureConformanceException() {
    }

    public ImpossibilityToMeasureConformanceException(String text) {
        super(text);
    }
}
