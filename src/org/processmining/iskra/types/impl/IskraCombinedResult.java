package org.processmining.iskra.types.impl;

import org.processmining.iskra.infrastucture.impl.IskraRepairChainImpl;
import org.processmining.iskra.types.AbstractIskraCombinedResult;
import org.processmining.iskra.types.IskraResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 07.07.14.
 */
public class IskraCombinedResult implements AbstractIskraCombinedResult {
    private final Map<IskraRepairChainImpl, IskraResult> result;

    public IskraCombinedResult(List<IskraRepairChainImpl> chains) {
        result = new HashMap<IskraRepairChainImpl, IskraResult>(chains.size());
    }

    @Override
    public Map<IskraRepairChainImpl, IskraResult> getResults() {
        return result;
    }
}
