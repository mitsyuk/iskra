package org.processmining.iskra.types.impl;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.iskra.types.DecomposedModel;
import org.processmining.log.models.EventLogArray;

public class DecomposedModelImpl implements DecomposedModel {

    private AcceptingPetriNetArray modelParts;
    private EventLogArray logParts;

    public DecomposedModelImpl(AcceptingPetriNetArray modelParts, EventLogArray logParts) {
        this.modelParts = modelParts;
        this.logParts = logParts;
    }

    @Override
    public AcceptingPetriNetArray getNetArray() {
        return this.modelParts;
    }

    @Override
    public EventLogArray getLogArray() {
        return this.logParts;
    }

    @Override
    public int getSize()
    {
        return modelParts.getSize();
    }

}
