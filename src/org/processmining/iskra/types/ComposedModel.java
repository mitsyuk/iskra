package org.processmining.iskra.types;

import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;

/**
 * The basic idea of composing petri net and log
 */
public interface ComposedModel {

    /**
     * get stored net
     *
     * @return stored petri net
     */
    AcceptingPetriNet getNet();


    /**
     * get stored log
     *
     * @return stored XES log
     */
    XLog getLog();

    TransEvClassMapping getMapping();

    void setMapping(TransEvClassMapping mapping);
}
