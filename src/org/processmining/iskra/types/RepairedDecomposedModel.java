package org.processmining.iskra.types;

public interface RepairedDecomposedModel extends DecomposedModel {
    org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray getNetsAfterRepairing();//TODO исключительно для тестов
}
