package org.processmining.iskra.types;

/**
 * Created by Ivan Shugurov on 07.11.2014.
 */
public class FinalEvaluationResultBuilder {
    private double initialModelFitness = -1;
    private double repairedModelFitness = -1;
    private double initialModelPrecision = -1;
    private double repairedModelPrecision = -1;
    private double initialModelGeneralization = -1;
    private double repairedModelGeneralization = -1;
    private int initialModelComplexity = -1;
    private int repairedModelComplexity = -1;
    private double initialModelConformance = -1;
    private double repairedModelConformance = -1;
    private double similarity = -1;
    private double totalEvaluation = -1;
    private String chainName;
    public FinalEvaluationResultBuilder(String chainName) {
        this.chainName = chainName;
    }

    public FinalEvaluationResult build() {
        return new FinalEvaluationResultImpl(chainName, initialModelFitness, repairedModelFitness,
                initialModelPrecision, repairedModelPrecision,
                initialModelGeneralization, repairedModelGeneralization,
                initialModelComplexity, repairedModelComplexity,
                initialModelConformance, repairedModelConformance, similarity, totalEvaluation);
    }

    public FinalEvaluationResultBuilder initialModelFitness(double initialModelFitness) {
        this.initialModelFitness = initialModelFitness;
        return this;
    }

    public FinalEvaluationResultBuilder repairedModelFitness(double repairedModelFitness) {
        this.repairedModelFitness = repairedModelFitness;
        return this;
    }

    public FinalEvaluationResultBuilder initialModelPrecision(double initialModelPrecision) {
        this.initialModelPrecision = initialModelPrecision;
        return this;
    }

    public FinalEvaluationResultBuilder repairedModelPrecision(double repairedModelPrecision) {
        this.repairedModelPrecision = repairedModelPrecision;
        return this;
    }

    public FinalEvaluationResultBuilder initialModelGeneralization(double initialModelGeneralization) {
        this.initialModelGeneralization = initialModelGeneralization;
        return this;
    }

    public FinalEvaluationResultBuilder repairedModelGeneralization(double repairedModelGeneralization) {
        this.repairedModelGeneralization = repairedModelGeneralization;
        return this;
    }

    public FinalEvaluationResultBuilder initialModelComplexity(int initialModelComplexity) {
        this.initialModelComplexity = initialModelComplexity;
        return this;
    }

    public FinalEvaluationResultBuilder repairedModelComplexity(int repairedModelComplexity) {
        this.repairedModelComplexity = repairedModelComplexity;
        return this;
    }

    public FinalEvaluationResultBuilder repairedAndInitialModelSimilarity(double repairedAndInitialModelSimilarity) {
        this.similarity = repairedAndInitialModelSimilarity;
        return this;
    }

    public FinalEvaluationResultBuilder totalEvaluation(double totalEvaluation) {
        this.totalEvaluation = totalEvaluation;
        return this;
    }

    public FinalEvaluationResultBuilder initialModelConformance(double initialModelConformance) {
        this.initialModelConformance = initialModelConformance;
        return this;
    }

    public FinalEvaluationResultBuilder repairedModelConformance(double repairedModelConformance) {
        this.repairedModelConformance = repairedModelConformance;
        return this;
    }

    public double getRepairedModelFitness() {
        return repairedModelFitness;
    }

    public double getRepairedModelPrecision() {
        return repairedModelPrecision;
    }

    public double getRepairedModelGeneralization() {
        return repairedModelGeneralization;
    }

    private static class FinalEvaluationResultImpl implements FinalEvaluationResult {
        private final double repairedModelFitness;
        private final double repairedModelPrecision;
        private final double repairedModelGeneralization;
        private final int repairedModelComplexity;
        private final double repairedModelConformance;
        private final double initialModelFitness;
        private final double initialModelPrecision;
        private final double initialModelGeneralization;
        private final int initialModelComplexity;
        private final double initialModelConformance;
        private final double similarity;
        private final double totalEvaluation;
        private final String chainName;

        private FinalEvaluationResultImpl(String chainName, double initialModelFitness, double repairedModelFitness,
                                          double initialModelPrecision, double repairedModelPrecision,
                                          double initialModelGeneralization, double repairedModelGeneralization,
                                          int initialModelComplexity, int repairedModelComplexity,
                                          double initialModelConformance, double repairedModelConformance,
                                          double similarity, double totalEvaluation) {
            this.chainName = chainName;
            this.repairedModelFitness = repairedModelFitness;
            this.repairedModelPrecision = repairedModelPrecision;
            this.repairedModelGeneralization = repairedModelGeneralization;
            this.repairedModelComplexity = repairedModelComplexity;
            this.initialModelFitness = initialModelFitness;
            this.initialModelPrecision = initialModelPrecision;
            this.initialModelGeneralization = initialModelGeneralization;
            this.initialModelComplexity = initialModelComplexity;
            this.repairedModelConformance = repairedModelConformance;
            this.initialModelConformance = initialModelConformance;
            this.similarity = similarity;
            this.totalEvaluation = totalEvaluation;
        }

        @Override
        public double getRepairedModelFitness() {
            return repairedModelFitness;
        }

        @Override
        public double getRepairedModelPrecision() {
            return repairedModelPrecision;
        }

        @Override
        public double getRepairedModelGeneralization() {
            return repairedModelGeneralization;
        }

        @Override
        public int getRepairedModelComplexity() {
            return repairedModelComplexity;
        }

        @Override
        public double getRepairedModelConformance() {
            return repairedModelConformance;
        }

        @Override
        public double getSimilarity() {
            return similarity;
        }

        @Override
        public double getInitialModelFitness() {
            return initialModelFitness;
        }

        @Override
        public double getInitialModelPrecision() {
            return initialModelPrecision;
        }

        @Override
        public double getInitialModelGeneralization() {
            return initialModelGeneralization;
        }

        @Override
        public int getInitialModelComplexity() {
            return initialModelComplexity;
        }

        @Override
        public double getInitialModelConformance() {
            return initialModelConformance;
        }

        @Override
        public double getTotalEvaluation() {
            return totalEvaluation;
        }

        @Override
        public String getChainName() {
            return chainName;
        }

    }
}
