package org.processmining.iskra.types;


import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.log.models.EventLogArray;

/**
 * Storage for net and log array
 */
public interface DecomposedModel {

    /**
     * Method for getting net array
     *
     * @return net array
     */
    AcceptingPetriNetArray getNetArray();

    /**
     * Method for getting log array
     *
     * @return log array
     */
    EventLogArray getLogArray();

    int getSize();

}
