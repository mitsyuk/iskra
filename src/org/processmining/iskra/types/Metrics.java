package org.processmining.iskra.types;

import org.processmining.iskra.pluginwraps.conformance.Conformance;

/**
 * Created by Ivan on 09.07.2015.
 */
public class Metrics {
    private double initialFitness = 1;
    private double finalFitness = 1;

    private double initialPrecision = 0;
    private double finalPrecision = 1;

    private double initialGeneralization = 0;
    private double finalGeneralization = 1;

    private double initialSimplicity = 1;
    private double finalSimplicity = 1;

    private double similarity = 1;

    public double getInitialFitness() {
        return initialFitness;
    }

    public boolean setInitialFitness(double initialFitness) {
        boolean correctValue = checkRange(initialFitness);
        if (correctValue) {
            this.initialFitness = initialFitness;
        }

        return correctValue;
    }

    public double getFinalFitness() {
        return finalFitness;
    }

    public boolean setFinalFitness(double finalFitness) {
        boolean correctValue = checkRange(finalFitness);

        if (correctValue) {
            this.finalFitness = finalFitness;
        }

        return correctValue;
    }

    public double getInitialPrecision() {
        return initialPrecision;
    }

    public boolean setInitialPrecision(double initialPrecision) {
        boolean correctValue = checkRange(initialPrecision);

        if (correctValue) {
            this.initialPrecision = initialPrecision;
        }

        return correctValue;
    }

    public double getFinalPrecision() {
        return finalPrecision;
    }

    public boolean setFinalPrecision(double finalPrecision) {
        boolean correctValue = checkRange(finalPrecision);

        if (correctValue) {
            this.finalPrecision = finalPrecision;
        }

        return correctValue;
    }

    public double getInitialGeneralization() {
        return initialGeneralization;
    }

    public boolean setInitialGeneralization(double initialGeneralization) {
        boolean correctValue = checkRange(initialGeneralization);

        if (correctValue) {
            this.initialGeneralization = initialGeneralization;
        }

        return correctValue;
    }

    public double getFinalGeneralization() {
        return finalGeneralization;
    }

    public boolean setFinalGeneralization(double finalGeneralization) {
        boolean correctValue = checkRange(finalGeneralization);

        if (correctValue) {
            this.finalGeneralization = finalGeneralization;
        }

        return correctValue;
    }

    public double getInitialSimplicity() {
        return initialSimplicity;
    }

    public boolean setInitialSimplicity(double initialSimplicity) {
        boolean correctValue = checkRange(initialSimplicity);

        if (correctValue) {
            this.initialSimplicity = initialSimplicity;
        }

        return correctValue;
    }

    public double getFinalSimplicity() {
        return finalSimplicity;
    }

    public boolean setFinalSimplicity(double finalSimplicity) {
        boolean correctValue = checkRange(finalSimplicity);

        if (correctValue) {
            this.finalSimplicity = finalSimplicity;
        }

        return correctValue;
    }

    public double getSimilarity() {
        return similarity;
    }

    public boolean setSimilarity(double similarity) {
        boolean correctValue = checkRange(similarity);

        if (correctValue) {
            this.similarity = similarity;
        }

        return correctValue;
    }

    private boolean checkRange(double value) {
        return !(value < 0 || value > 1);
    }

    public boolean measureOnlyFitness() {
        return initialPrecision < Double.MIN_VALUE && initialGeneralization < Double.MIN_VALUE;
    }

    public boolean measureAllMetrics() {
        return !measureOnlyFitness();
    }

    public double evaluateInitialModel(Conformance conformance) {
        return evaluateInitialModel(conformance.getFitness(), conformance.getGeneralization(), conformance.getPrecision());
    }

    public double evaluateInitialModel(double fitness, double generalization, double precision) {
        return (fitness * initialFitness + generalization * initialGeneralization +
                precision * initialPrecision) / (initialFitness + initialGeneralization + initialPrecision);
    }

    public double evaluateFinalModel(Conformance conformance) {
        return evaluateFinalModel(conformance.getFitness(), conformance.getGeneralization(), conformance.getPrecision());
    }

    public double evaluateFinalModel(double fitness, double generalization, double precision) {
        return (fitness * finalFitness + generalization * finalGeneralization +
                precision * finalPrecision) / (finalFitness + finalGeneralization + finalPrecision);
    }
}
