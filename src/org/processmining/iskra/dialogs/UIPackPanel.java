package org.processmining.iskra.dialogs;

import org.processmining.iskra.infrastucture.impl.IskraRepairChainImpl;
import org.processmining.iskra.plugins.IskraChainGenerator;
import ru.hse.pais.shugurov.widgets.elements.ElementPanel;
import ru.hse.pais.shugurov.widgets.panels.EditableListPanel;

import java.util.List;

/**
 * Created by user on 07.07.14.
 */
public class UIPackPanel extends EditableListPanel<IskraRepairChainImpl> {

    public UIPackPanel(List<IskraRepairChainImpl> existentElements) {
        super(existentElements);
    }

    @Override
    protected IskraRepairChainImpl createNewElement() {
        return new IskraChainGenerator().generateUIWithDialog();
    }

    @Override
    protected boolean editElement(IskraRepairChainImpl iskraRepairChain, ElementPanel elementPanel) {
        return true;
    }
}
