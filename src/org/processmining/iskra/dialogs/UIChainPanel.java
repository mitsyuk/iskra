package org.processmining.iskra.dialogs;

import org.processmining.iskra.infrastucture.impl.IskraRepairChainImpl;

import javax.swing.*;

/**
 * Created by user on 07.07.14.
 */
public class UIChainPanel extends JPanel {
    public UIChainPanel(IskraRepairChainImpl chain) {
        add(new JLabel(chain.toString()));
    }
}
