package org.processmining.iskra.utils;

import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.classification.XEventClasses;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryNaiveImpl;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.jdom.adapters.XercesDOMAdapter;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.log.models.EventLogArray;
import org.processmining.log.models.impl.EventLogArrayFactory;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Arc;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;

import java.util.*;

/**
 * Created by Ivan on 05.05.2015.
 */
public class LogDecomposer {

    public EventLogArray decompose(AcceptingPetriNetArray netArray, XLog log, TransEvClassMapping mapping) {
        EventLogArray decomposedLogs = EventLogArrayFactory.createEventLogArray();
        Map<XEventClass, List<Integer>> labelsToIndexesOfNets = new HashMap<>();

        XFactory xFactory = new XFactoryNaiveImpl();

        HashMap<String, XEventClass> labelToEventMap = new HashMap<>();

        for (Map.Entry<Transition, XEventClass> pair : mapping.entrySet())
            labelToEventMap.put(pair.getKey().getAttributeMap().get(AttributeMap.LABEL).toString(), pair.getValue());

        for (int i = 0; i < netArray.getSize(); i++) {
            AcceptingPetriNet acceptingPetriNet = netArray.getNet(i);
            Petrinet petrinet = acceptingPetriNet.getNet();

            for (Transition transition : petrinet.getTransitions()) {
                if (transition.isInvisible()) {
                    continue;
                }

                XEventClass currentEventClass = labelToEventMap.get(PlusRemoval.removeLastPlus(transition.getLabel()));

                List<Integer> indexesOfNetsWithThisTransition = labelsToIndexesOfNets.get(currentEventClass);

                if (indexesOfNetsWithThisTransition == null) {
                    indexesOfNetsWithThisTransition = new ArrayList<>();
                    labelsToIndexesOfNets.put(currentEventClass, indexesOfNetsWithThisTransition);
                }

                indexesOfNetsWithThisTransition.add(i);
            }

            XLog xLog = xFactory.createLog();
            decomposedLogs.addLog(xLog);
        }

        XConceptExtension conceptExtension = XConceptExtension.instance();

        XEventClasses eventClasses = XLogInfoFactory.createLogInfo(log, mapping.getEventClassifier()).getEventClasses();

        for (XTrace trace : log) {
            String traceName = conceptExtension.extractName(trace);

            XTrace[] decomposedTraces = new XTrace[netArray.getSize()];
            for (int i = 0; i < decomposedTraces.length; i++) {
                decomposedTraces[i] = xFactory.createTrace();
                conceptExtension.assignName(decomposedTraces[i], traceName);
            }

            for (XEvent event : trace) {
                List<Integer> indexesOfNetsWithThisEvent = labelsToIndexesOfNets.get(eventClasses.getClassOf(event));
                if (indexesOfNetsWithThisEvent == null) {
                    continue;
                }

                for (int i : indexesOfNetsWithThisEvent) {
                    XEvent eventCopy = (XEvent) event.clone();
                    decomposedTraces[i].add(eventCopy);
                }
            }

            for (int i = 0; i < decomposedTraces.length; i++) {
                XTrace decomposedTrace = decomposedTraces[i];

                if (!decomposedTrace.isEmpty())
                    decomposedLogs.getLog(i).add(decomposedTrace);
            }
        }

        return decomposedLogs;
    }

}