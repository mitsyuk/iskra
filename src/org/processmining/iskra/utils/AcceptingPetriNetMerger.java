package org.processmining.iskra.utils;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.plugins.MergeAcceptingPetriNetArrayIntoAcceptingPetriNetPlugin;
import org.processmining.framework.plugin.PluginContext;

/**
 * Created by Ivan on 18.04.2016.
 */
public class AcceptingPetriNetMerger {
    public static AcceptingPetriNet mergePetriNetArray(PluginContext context, AcceptingPetriNetArray netArray) {
        AcceptingPetriNet mergedNet = new MergeAcceptingPetriNetArrayIntoAcceptingPetriNetPlugin().runDefault(
                context, netArray);
        return mergedNet;
    }
}