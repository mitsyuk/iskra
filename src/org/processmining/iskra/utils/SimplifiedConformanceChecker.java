package org.processmining.iskra.utils;

import org.deckfour.xes.model.XLog;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.types.DecomposedModel;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.plugins.dc.CheckDecomposedConformanceUsingSESEsPlugin;
import org.processmining.plugins.petrinet.replayresult.PNRepResult;

public class SimplifiedConformanceChecker {

    public static double getConformance(PluginContext context, Petrinet pnet, XLog log) {
        org.processmining.models.ReplayResultArray resultArray = (new CheckDecomposedConformanceUsingSESEsPlugin()).checkDefault(context, pnet, log);
        Double sum = 0.0;
        for (int i = 0; i < resultArray.getSize(); i++) {
            sum += (Double)resultArray.getReplay(i).getInfo().get(PNRepResult.MOVEMODELFITNESS);
        }

        return sum / resultArray.getSize();
    }

    public static double[] getDecomposedConformance(PluginContext context, DecomposedModel model) {
        double[] result = new double[model.getSize()];

        for (int i = 0; i < model.getSize(); i++) {
            result[i] = getConformance(context, model.getNetArray().getNet(i).getNet(), model.getLogArray().getLog(i));
        }

        return result;
    }
}