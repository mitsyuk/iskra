package org.processmining.iskra.utils;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryNaiveImpl;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;

import java.util.HashSet;
import java.util.Set;

import static org.processmining.iskra.utils.PlusRemoval.removePlus;

/**
 * Created by Ivan on 18.04.2016.
 */
public class LogProjection {

    public static XLog projectNetOnLog(Petrinet net, XLog log) {
        Set<String> labels = new HashSet<>();

        for (Transition transition : net.getTransitions()) {
            String label = transition.getLabel();
            label = removePlus(label);
            labels.add(label);
        }

        return projectNetOnLog(labels, log);
    }

    public static XLog projectNetOnLog(Set<String> labels, XLog log) {
        XFactory factory = new XFactoryNaiveImpl();
        XLog projection = factory.createLog();

        XConceptExtension conceptExtension = XConceptExtension.instance();
        for (XTrace originalTrace : log) {
            XTrace projectedTrace = factory.createTrace((XAttributeMap) log.getAttributes().clone());

            for (XEvent event : originalTrace) {
                String eventName = conceptExtension.extractName(event);
                eventName = removePlus(eventName);

                if (labels.contains(eventName)) {
                    projectedTrace.add((XEvent) event.clone());
                }
            }

            if (!projectedTrace.isEmpty()) {
                projection.add(projectedTrace);
            }
        }

        return projection;
    }
}
