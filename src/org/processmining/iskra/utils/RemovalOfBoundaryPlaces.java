package org.processmining.iskra.utils;

import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;

import java.util.ArrayList;
import java.util.List;

import static org.processmining.iskra.utils.DanglingTransitionsRemover.removeDanglingTransitions;

/**
 * Created by Ivan on 12.07.2016.
 */
public class RemovalOfBoundaryPlaces {
    /**
     * removes all places without input out output flows
     *
     * @param petrinet
     * @param containsInitialPlaces
     * @param containsFinalPlaces
     */
    public static void removeBoundaryPlaces(Petrinet petrinet, boolean containsInitialPlaces, boolean containsFinalPlaces) {
        if (!containsInitialPlaces || !containsFinalPlaces) {

            List<Place> placesToBeRemoved = new ArrayList<Place>();

            for (Place place : petrinet.getPlaces()) {

                if (petrinet.getInEdges(place).isEmpty() || place.getLabel().equals("source")) {
                    //initial place
                    if (!containsInitialPlaces) {
                        placesToBeRemoved.add(place);
                    }
                }

                if (petrinet.getOutEdges(place).isEmpty() || place.getLabel().equals("sink")) {
                    //final place
                    if (!containsFinalPlaces) {
                        placesToBeRemoved.add(place);
                    }
                }
            }

            for (Place place : placesToBeRemoved) {
                petrinet.removePlace(place);
            }
        }

        removeDanglingTransitions(petrinet);
    }
}
