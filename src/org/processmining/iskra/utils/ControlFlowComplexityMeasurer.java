package org.processmining.iskra.utils;

import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetEdge;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;

import java.util.Collection;

/**
 * Created by Ivan on 14.07.2015.
 */
public class ControlFlowComplexityMeasurer {
    public int measure(Petrinet petrinet) {
        int complexity = 0;

        for (Place place : petrinet.getPlaces()) {
            Collection<PetrinetEdge<?, ?>> outgoingArcs = petrinet.getOutEdges(place);

            if (outgoingArcs.size() > 1) {
                complexity += outgoingArcs.size();
            }
        }

        for (Transition transition : petrinet.getTransitions()) {
            Collection<PetrinetEdge<?, ?>> outgoingArcs = petrinet.getOutEdges(transition);

            if (outgoingArcs.size() > 1) {
                complexity++;
            }
        }

        return complexity;
    }

}
