package org.processmining.iskra.utils;

import org.processmining.framework.util.Pair;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Ivan on 15.08.2016.
 */
public class BoundaryPlacesFinder {
    public Pair<Set<Place>, Set<Place>> find(Petrinet petrinet){
        Set<Place> initialPlaces = new HashSet<>();
        Set<Place> finalPlaces = new HashSet<>();

        for (Place place : petrinet.getPlaces()){
            if (petrinet.getInEdges(place).isEmpty()){
                initialPlaces.add(place);
            }

            if (petrinet.getOutEdges(place).isEmpty()){
                finalPlaces.add(place);
            }
        }

        return new Pair<>(initialPlaces, finalPlaces);
    }
}
